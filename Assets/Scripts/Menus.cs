﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menus : MonoBehaviour
{

	[SerializeField]
	private GameObject mainMenuUI;
	[SerializeField]
	private GameObject gameplayUI;
	[SerializeField]
	private GameObject pauseMenuUI;
	[SerializeField]
	private GameObject gameOverMenuUI;
	[SerializeField]
	private GameObject mainButtons;

	[SerializeField]
	private BallsPathfinder pathFinder;
	private AudioSource buttonSound;

	[SerializeField]
	private Sprite[] balls;
	[SerializeField]
	private Image[] nextWaveBalls;
	[SerializeField]
	private Text score;
	[SerializeField]
	private Text bestScore;

	[SerializeField]
	private Text timer;
	private float seconds;
	private int minutes;
	private int hour;

	void Start()
	{
		buttonSound = GameObject.Find("ButtonSound").GetComponent<AudioSource>();
	}


	void UpdateTimer()
	{
		seconds += Time.deltaTime;
		timer.text = hour + ":" + minutes.ToString("00") + ":" + ((int)seconds).ToString("00");
		if (seconds >= 60)
		{
			minutes++;
			seconds = 0;
		}
		else if (minutes >= 60)
		{
			hour++;
			minutes = 0;
		}
	}

	void ResetTimer()
	{
		seconds = 0.0f;
		minutes = 0;
		hour = 0;

		UpdateTimer();
	}

	private void Update()
	{
		UpdateTimer();
	}

	public void StartGame()
	{
		mainMenuUI.SetActive(false);
		gameplayUI.SetActive(true);
		pathFinder.enabled = true;
		score.text = "SCORE: " + Vars.score;
		bestScore.text = "BEST: " + PlayerPrefs.GetInt("BestScore");
		pathFinder.numberOfRows = 9;
		pathFinder.numberOfColumns = 9;
	}

	public void ExitGame()
	{
		Application.Quit();
	}

	public void PauseMenu()
	{
		buttonSound.Play();
		pauseMenuUI.SetActive(true);
	}

	public void ClosePauseMenu()
	{
		buttonSound.Play();
		pauseMenuUI.SetActive(false);
	}

	public void GameOverMenu()
	{
		gameOverMenuUI.SetActive(true);
	}

	public void RestartTheGame()
	{
		pathFinder.enabled = false;
		DestroyAllTiles();
		Vars.ResetAllVariables();
		pauseMenuUI.SetActive(false);
		gameOverMenuUI.SetActive(false);
		ResetTimer();
		Invoke("Reset", 0);
	}

	private void Reset()
	{
		StartGame();
	}

	private void DestroyAllTiles()
	{
		Transform tilesGameObject = GameObject.Find("Tiles").transform;
		foreach (Transform child in tilesGameObject)
		{
			GameObject.Destroy(child.gameObject);
		}
	}

	public void BackToTheMainMenu()
	{
		DestroyAllTiles();
		mainMenuUI.SetActive(true);
		gameplayUI.SetActive(false);
		pathFinder.enabled = false;
		Vars.ResetAllVariables();
		mainButtons.transform.localScale = new Vector2(1, 1);
		pauseMenuUI.SetActive(false);
		gameOverMenuUI.SetActive(false);
	}

	public void UpdateNextWaveBallsColor(int ball, int color)
	{
		nextWaveBalls[ball].sprite = balls[color];
	}

	public void UpdateScore()
	{
		score.text = "SCORE: " + Vars.score;
		if (Vars.score > PlayerPrefs.GetInt("BestScore"))
		{
			PlayerPrefs.SetInt("BestScore", Vars.score);
			bestScore.text = "BEST: " + PlayerPrefs.GetInt("BestScore");
		}
	}
}
