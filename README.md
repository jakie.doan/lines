# Lines



## Getting started

 ```
cd existing_repo
git remote add origin https://gitlab.com/jakie.doan/lines.git
git branch -M main
git push -uf origin main
```

## Name
Lines

## Description
Lines Gameplay.

## Run .exe
Open Build folder to run the game .exe file.

# Demo
![image](https://gitlab.com/jakie.doan/lines/-/raw/main/Demo/Demo.gif)
