﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BallExplosion::ActivateBallExplosion()
extern void BallExplosion_ActivateBallExplosion_mA79BA98CFE9D22708F489CE5561504B2F9486DD5 (void);
// 0x00000002 System.Void BallExplosion::.ctor()
extern void BallExplosion__ctor_mB0CC125BBE27181A04CA95DF3D6E0819E3703FD4 (void);
// 0x00000003 System.Void BallsPathfinder::OnEnable()
extern void BallsPathfinder_OnEnable_m23A5C290F0765720298791359A18F5BB6BA5049E (void);
// 0x00000004 System.Void BallsPathfinder::Initializefields(System.Int32,System.Int32)
extern void BallsPathfinder_Initializefields_m7B5B1A05A8426BCFF9B25CA75AC578001260F627 (void);
// 0x00000005 System.Void BallsPathfinder::CreateTiles()
extern void BallsPathfinder_CreateTiles_mDB15E60160F8C59BFFE014A9A048DB9292D18D91 (void);
// 0x00000006 System.Void BallsPathfinder::CreateNewBalls()
extern void BallsPathfinder_CreateNewBalls_m39799563225761B4B7D54ECDD5BB1BCA9AAE76DD (void);
// 0x00000007 System.Void BallsPathfinder::CreateNewBall(System.Int32,System.Int32)
extern void BallsPathfinder_CreateNewBall_m29425C07E0D9CE01502985D39A7ED770E2D94279 (void);
// 0x00000008 System.Int32 BallsPathfinder::CheckForAvailabeFields(System.Int32)
extern void BallsPathfinder_CheckForAvailabeFields_m65E0E87FFECB82698062D1E6B1C0F6D9DA455CAB (void);
// 0x00000009 System.Void BallsPathfinder::DestoyBall(System.Int32,System.Int32)
extern void BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE (void);
// 0x0000000A System.Void BallsPathfinder::CreatePlaceholderBalls(System.Int32)
extern void BallsPathfinder_CreatePlaceholderBalls_mF9E3691BB672251EBF290FFA0C6C347C7799A269 (void);
// 0x0000000B System.Void BallsPathfinder::ConvertPlaceholderBallsToRealBalls()
extern void BallsPathfinder_ConvertPlaceholderBallsToRealBalls_mB70FFEBD19BFC154933C9556587A73E88D9D3099 (void);
// 0x0000000C System.Void BallsPathfinder::CheckForScoreVertically()
extern void BallsPathfinder_CheckForScoreVertically_m77901B14D506A171956F78185326656C51FB0785 (void);
// 0x0000000D System.Void BallsPathfinder::CheckForScoreHorizontally()
extern void BallsPathfinder_CheckForScoreHorizontally_m079A65463A19BB59EA49B8C9530061206A2BFAC1 (void);
// 0x0000000E System.Void BallsPathfinder::CheckForScoreDiagonally()
extern void BallsPathfinder_CheckForScoreDiagonally_m0D8C80E75B167112F06671DDE6175BF698820B80 (void);
// 0x0000000F System.Boolean BallsPathfinder::CheckForScoreDiagonallyTraverse(System.Int32[,],System.Int32,System.Int32,System.Int32,System.Int32)
extern void BallsPathfinder_CheckForScoreDiagonallyTraverse_m38B44ED6144820ABF5C94721B87FC4B475D84C7A (void);
// 0x00000010 System.Void BallsPathfinder::CheckScore()
extern void BallsPathfinder_CheckScore_m6345AE27461D331492D27D28E16945CCF09411F5 (void);
// 0x00000011 System.Void BallsPathfinder::ResetConsecuteveBallsSearchingVariables()
extern void BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6 (void);
// 0x00000012 System.Void BallsPathfinder::InitializeBallMovement(UnityEngine.GameObject,System.Int32,System.Int32)
extern void BallsPathfinder_InitializeBallMovement_m9E10C61F99868F73E615449DF46689DD33CD340B (void);
// 0x00000013 System.Collections.IEnumerator BallsPathfinder::BallMovement(System.Int32[,],System.Int32,System.Int32,System.Int32)
extern void BallsPathfinder_BallMovement_m48DD0BF5B50C07413E4D143C31463126971EEB49 (void);
// 0x00000014 System.Int32 BallsPathfinder::get_Rows()
extern void BallsPathfinder_get_Rows_mE5A605AC48FA7C0E3602B21226B15FC04F63B13B (void);
// 0x00000015 System.Int32 BallsPathfinder::get_Cols()
extern void BallsPathfinder_get_Cols_m2D237BCDE09D2ED862D8855D71B86BDBCDE01C14 (void);
// 0x00000016 System.Int32 BallsPathfinder::GetNodeContents(System.Int32[,],System.Int32)
extern void BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260 (void);
// 0x00000017 System.Void BallsPathfinder::ChangeNodeContents(System.Int32[,],System.Int32,System.Int32)
extern void BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783 (void);
// 0x00000018 System.Int32[,] BallsPathfinder::FindPath(System.Int32,System.Int32,System.Int32,System.Int32)
extern void BallsPathfinder_FindPath_m7FA847FADB1462CBBB01437FE18C89FFD28B5597 (void);
// 0x00000019 System.Int32[,] BallsPathfinder::Search(System.Int32,System.Int32)
extern void BallsPathfinder_Search_m29FA4638DD4989B518626DC247A9CCCE00CD7F72 (void);
// 0x0000001A System.Void BallsPathfinder::.ctor()
extern void BallsPathfinder__ctor_m916FD55F58B61026AEFA0DF2573F23A3DB73C54E (void);
// 0x0000001B System.Void BallsPathfinder/<BallMovement>d__29::.ctor(System.Int32)
extern void U3CBallMovementU3Ed__29__ctor_mE3D83FBDE142D6E79AC3714B748B82A3379152B1 (void);
// 0x0000001C System.Void BallsPathfinder/<BallMovement>d__29::System.IDisposable.Dispose()
extern void U3CBallMovementU3Ed__29_System_IDisposable_Dispose_m91C328B8B1C9304150F270CAD73CE211EC479834 (void);
// 0x0000001D System.Boolean BallsPathfinder/<BallMovement>d__29::MoveNext()
extern void U3CBallMovementU3Ed__29_MoveNext_mC5B4B894C674149A5F1FDBF01B9A842712143402 (void);
// 0x0000001E System.Object BallsPathfinder/<BallMovement>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBallMovementU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52A687D8880551C632BFC1DF724CC1315A100569 (void);
// 0x0000001F System.Void BallsPathfinder/<BallMovement>d__29::System.Collections.IEnumerator.Reset()
extern void U3CBallMovementU3Ed__29_System_Collections_IEnumerator_Reset_m34433EBCFA168D9DDA8E486D0BADD873ACCAC264 (void);
// 0x00000020 System.Object BallsPathfinder/<BallMovement>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CBallMovementU3Ed__29_System_Collections_IEnumerator_get_Current_mFE6FF4878CA4A2449D73AF6D2CC7A747061D28F3 (void);
// 0x00000021 System.Void DestroyExplosionParticle::OnEnable()
extern void DestroyExplosionParticle_OnEnable_mEE71A98ADC6316C035FFE6E7AFD26C760EBF7C3A (void);
// 0x00000022 System.Void DestroyExplosionParticle::.ctor()
extern void DestroyExplosionParticle__ctor_m99F38967986788A00A1FFDE267649E2A6FDE4852 (void);
// 0x00000023 System.Void Menus::Start()
extern void Menus_Start_m757D84C515E8782E8A19DC7833F8BF0A15163499 (void);
// 0x00000024 System.Void Menus::UpdateTimer()
extern void Menus_UpdateTimer_mB8AD879D0B4372CD0C5F32370D09D81096481148 (void);
// 0x00000025 System.Void Menus::ResetTimer()
extern void Menus_ResetTimer_m5A959F0FF4A0C9A61F36E21DB58E70EA725D0D18 (void);
// 0x00000026 System.Void Menus::Update()
extern void Menus_Update_m48773A3B354E97888B0578CD3FE8DC889B7BD2A7 (void);
// 0x00000027 System.Void Menus::StartGame()
extern void Menus_StartGame_m2B84735B2B5A9DA804212E62FBFF2E9FC7F69B21 (void);
// 0x00000028 System.Void Menus::ExitGame()
extern void Menus_ExitGame_m998AC26A505A814417AB0ACD87A1D612FC7D0521 (void);
// 0x00000029 System.Void Menus::PauseMenu()
extern void Menus_PauseMenu_mD2F16EB6B188ACC9B67579DF10D152C1343A5224 (void);
// 0x0000002A System.Void Menus::ClosePauseMenu()
extern void Menus_ClosePauseMenu_mDC94D204451B98F7A1D3843D54F73316AFC92CE5 (void);
// 0x0000002B System.Void Menus::GameOverMenu()
extern void Menus_GameOverMenu_m55C974EC861F3C28CB8BAAF89883DF51C0D291F7 (void);
// 0x0000002C System.Void Menus::RestartTheGame()
extern void Menus_RestartTheGame_m583C0A2D7D72C41F8D91B5B0492B74F7CCDBD69B (void);
// 0x0000002D System.Void Menus::Reset()
extern void Menus_Reset_m5CD3E64A48477CCE622CF53F3D3BD135377D08A0 (void);
// 0x0000002E System.Void Menus::DestroyAllTiles()
extern void Menus_DestroyAllTiles_m1D62BB3B9F661210BF1800BC086A4E4AC5558891 (void);
// 0x0000002F System.Void Menus::BackToTheMainMenu()
extern void Menus_BackToTheMainMenu_m66E0F80462B2363911C0D06CD5675793BD536FF8 (void);
// 0x00000030 System.Void Menus::UpdateNextWaveBallsColor(System.Int32,System.Int32)
extern void Menus_UpdateNextWaveBallsColor_mDDCDA9C3B75918F26B537B10C3C97DFC272BE1A5 (void);
// 0x00000031 System.Void Menus::UpdateScore()
extern void Menus_UpdateScore_m6D7C68AE360246A76F901FE59837276BF601E8DC (void);
// 0x00000032 System.Void Menus::.ctor()
extern void Menus__ctor_mBD8483151FBC884103329A35949A7354D29BED9B (void);
// 0x00000033 System.Void SelectedBallAnimation::OnEnable()
extern void SelectedBallAnimation_OnEnable_m11E98BDF5DF733E96C3F6BA2FF70A33391AB9004 (void);
// 0x00000034 System.Void SelectedBallAnimation::Update()
extern void SelectedBallAnimation_Update_mEE60CBB1A15C030ACC7D403ADCCCB38D5152E649 (void);
// 0x00000035 System.Void SelectedBallAnimation::.ctor()
extern void SelectedBallAnimation__ctor_m9CA2B55CB46169D9D43EEDF4582B9A52E776BD3B (void);
// 0x00000036 System.Void Tile::OnMouseDown()
extern void Tile_OnMouseDown_m15CC374FC44376602BDD7405A7125F9AB2542ED0 (void);
// 0x00000037 System.Void Tile::.ctor()
extern void Tile__ctor_mB2C904B47040471552C938AE751AC0BF80B369E7 (void);
// 0x00000038 System.Void Vars::ResetAllVariables()
extern void Vars_ResetAllVariables_m038B64E49DA06DAC269D70A416CD22A1B6A80D9D (void);
// 0x00000039 System.Void Vars::.ctor()
extern void Vars__ctor_mE9D35ECC8033B60F9515456B41A5389407F0E43C (void);
// 0x0000003A System.Void Vars::.cctor()
extern void Vars__cctor_m82F628B1BC3817FC1D99FF688094BC0B3F5C010F (void);
// 0x0000003B System.Void ZoomInAnimation::OnEnable()
extern void ZoomInAnimation_OnEnable_mEE1B958AC38DF423FE2938D26828475E1A9A0212 (void);
// 0x0000003C System.Void ZoomInAnimation::Update()
extern void ZoomInAnimation_Update_mBB3D4C37ACD4A99B6371A93372701BBAA9BB5D5D (void);
// 0x0000003D System.Void ZoomInAnimation::.ctor()
extern void ZoomInAnimation__ctor_m30A2A63AAD67E93737FAA34F1FCBA885D8A913FB (void);
// 0x0000003E System.Void ZoomOutAnimation::OnEnable()
extern void ZoomOutAnimation_OnEnable_mAB26ED292376031B221E40BE0B1D813CA451711B (void);
// 0x0000003F System.Void ZoomOutAnimation::Update()
extern void ZoomOutAnimation_Update_mBF9261FCF85B767BBCD69F83E41EBC5B2933B9A2 (void);
// 0x00000040 System.Void ZoomOutAnimation::.ctor()
extern void ZoomOutAnimation__ctor_m1D7BF514B776D4FC62A3755AB01B0B7CEFBA139D (void);
static Il2CppMethodPointer s_methodPointers[64] = 
{
	BallExplosion_ActivateBallExplosion_mA79BA98CFE9D22708F489CE5561504B2F9486DD5,
	BallExplosion__ctor_mB0CC125BBE27181A04CA95DF3D6E0819E3703FD4,
	BallsPathfinder_OnEnable_m23A5C290F0765720298791359A18F5BB6BA5049E,
	BallsPathfinder_Initializefields_m7B5B1A05A8426BCFF9B25CA75AC578001260F627,
	BallsPathfinder_CreateTiles_mDB15E60160F8C59BFFE014A9A048DB9292D18D91,
	BallsPathfinder_CreateNewBalls_m39799563225761B4B7D54ECDD5BB1BCA9AAE76DD,
	BallsPathfinder_CreateNewBall_m29425C07E0D9CE01502985D39A7ED770E2D94279,
	BallsPathfinder_CheckForAvailabeFields_m65E0E87FFECB82698062D1E6B1C0F6D9DA455CAB,
	BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE,
	BallsPathfinder_CreatePlaceholderBalls_mF9E3691BB672251EBF290FFA0C6C347C7799A269,
	BallsPathfinder_ConvertPlaceholderBallsToRealBalls_mB70FFEBD19BFC154933C9556587A73E88D9D3099,
	BallsPathfinder_CheckForScoreVertically_m77901B14D506A171956F78185326656C51FB0785,
	BallsPathfinder_CheckForScoreHorizontally_m079A65463A19BB59EA49B8C9530061206A2BFAC1,
	BallsPathfinder_CheckForScoreDiagonally_m0D8C80E75B167112F06671DDE6175BF698820B80,
	BallsPathfinder_CheckForScoreDiagonallyTraverse_m38B44ED6144820ABF5C94721B87FC4B475D84C7A,
	BallsPathfinder_CheckScore_m6345AE27461D331492D27D28E16945CCF09411F5,
	BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6,
	BallsPathfinder_InitializeBallMovement_m9E10C61F99868F73E615449DF46689DD33CD340B,
	BallsPathfinder_BallMovement_m48DD0BF5B50C07413E4D143C31463126971EEB49,
	BallsPathfinder_get_Rows_mE5A605AC48FA7C0E3602B21226B15FC04F63B13B,
	BallsPathfinder_get_Cols_m2D237BCDE09D2ED862D8855D71B86BDBCDE01C14,
	BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260,
	BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783,
	BallsPathfinder_FindPath_m7FA847FADB1462CBBB01437FE18C89FFD28B5597,
	BallsPathfinder_Search_m29FA4638DD4989B518626DC247A9CCCE00CD7F72,
	BallsPathfinder__ctor_m916FD55F58B61026AEFA0DF2573F23A3DB73C54E,
	U3CBallMovementU3Ed__29__ctor_mE3D83FBDE142D6E79AC3714B748B82A3379152B1,
	U3CBallMovementU3Ed__29_System_IDisposable_Dispose_m91C328B8B1C9304150F270CAD73CE211EC479834,
	U3CBallMovementU3Ed__29_MoveNext_mC5B4B894C674149A5F1FDBF01B9A842712143402,
	U3CBallMovementU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52A687D8880551C632BFC1DF724CC1315A100569,
	U3CBallMovementU3Ed__29_System_Collections_IEnumerator_Reset_m34433EBCFA168D9DDA8E486D0BADD873ACCAC264,
	U3CBallMovementU3Ed__29_System_Collections_IEnumerator_get_Current_mFE6FF4878CA4A2449D73AF6D2CC7A747061D28F3,
	DestroyExplosionParticle_OnEnable_mEE71A98ADC6316C035FFE6E7AFD26C760EBF7C3A,
	DestroyExplosionParticle__ctor_m99F38967986788A00A1FFDE267649E2A6FDE4852,
	Menus_Start_m757D84C515E8782E8A19DC7833F8BF0A15163499,
	Menus_UpdateTimer_mB8AD879D0B4372CD0C5F32370D09D81096481148,
	Menus_ResetTimer_m5A959F0FF4A0C9A61F36E21DB58E70EA725D0D18,
	Menus_Update_m48773A3B354E97888B0578CD3FE8DC889B7BD2A7,
	Menus_StartGame_m2B84735B2B5A9DA804212E62FBFF2E9FC7F69B21,
	Menus_ExitGame_m998AC26A505A814417AB0ACD87A1D612FC7D0521,
	Menus_PauseMenu_mD2F16EB6B188ACC9B67579DF10D152C1343A5224,
	Menus_ClosePauseMenu_mDC94D204451B98F7A1D3843D54F73316AFC92CE5,
	Menus_GameOverMenu_m55C974EC861F3C28CB8BAAF89883DF51C0D291F7,
	Menus_RestartTheGame_m583C0A2D7D72C41F8D91B5B0492B74F7CCDBD69B,
	Menus_Reset_m5CD3E64A48477CCE622CF53F3D3BD135377D08A0,
	Menus_DestroyAllTiles_m1D62BB3B9F661210BF1800BC086A4E4AC5558891,
	Menus_BackToTheMainMenu_m66E0F80462B2363911C0D06CD5675793BD536FF8,
	Menus_UpdateNextWaveBallsColor_mDDCDA9C3B75918F26B537B10C3C97DFC272BE1A5,
	Menus_UpdateScore_m6D7C68AE360246A76F901FE59837276BF601E8DC,
	Menus__ctor_mBD8483151FBC884103329A35949A7354D29BED9B,
	SelectedBallAnimation_OnEnable_m11E98BDF5DF733E96C3F6BA2FF70A33391AB9004,
	SelectedBallAnimation_Update_mEE60CBB1A15C030ACC7D403ADCCCB38D5152E649,
	SelectedBallAnimation__ctor_m9CA2B55CB46169D9D43EEDF4582B9A52E776BD3B,
	Tile_OnMouseDown_m15CC374FC44376602BDD7405A7125F9AB2542ED0,
	Tile__ctor_mB2C904B47040471552C938AE751AC0BF80B369E7,
	Vars_ResetAllVariables_m038B64E49DA06DAC269D70A416CD22A1B6A80D9D,
	Vars__ctor_mE9D35ECC8033B60F9515456B41A5389407F0E43C,
	Vars__cctor_m82F628B1BC3817FC1D99FF688094BC0B3F5C010F,
	ZoomInAnimation_OnEnable_mEE1B958AC38DF423FE2938D26828475E1A9A0212,
	ZoomInAnimation_Update_mBB3D4C37ACD4A99B6371A93372701BBAA9BB5D5D,
	ZoomInAnimation__ctor_m30A2A63AAD67E93737FAA34F1FCBA885D8A913FB,
	ZoomOutAnimation_OnEnable_mAB26ED292376031B221E40BE0B1D813CA451711B,
	ZoomOutAnimation_Update_mBF9261FCF85B767BBCD69F83E41EBC5B2933B9A2,
	ZoomOutAnimation__ctor_m1D7BF514B776D4FC62A3755AB01B0B7CEFBA139D,
};
static const int32_t s_InvokerIndices[64] = 
{
	3146,
	3146,
	3146,
	1324,
	3146,
	3146,
	1324,
	1807,
	1324,
	2551,
	3146,
	3146,
	3146,
	3146,
	247,
	3146,
	3146,
	779,
	444,
	3048,
	3048,
	882,
	779,
	441,
	981,
	3146,
	2551,
	3146,
	3097,
	3066,
	3146,
	3066,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	1324,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
	4701,
	3146,
	4701,
	3146,
	3146,
	3146,
	3146,
	3146,
	3146,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	64,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
