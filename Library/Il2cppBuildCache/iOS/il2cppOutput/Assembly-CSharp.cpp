﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tE6BB71ABF15905EFA2BE92C38A2716547AEADB19;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer>
struct UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t8869694C217655DA7B1315DC02C80F1308B78B78;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// System.Type[]
struct TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// System.Int32[,]
struct Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F;
// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299;
// BallExplosion
struct BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850;
// BallsPathfinder
struct BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990;
// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA;
// System.Reflection.Binder
struct Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235;
// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// DestroyExplosionParticle
struct DestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4;
// UnityEngine.UI.FontData
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// System.Reflection.MemberFilter
struct MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553;
// Menus
struct Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// SelectedBallAnimation
struct SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// Tile
struct Tile_t192D2F5511792792FB74C37341AFAA9F6B77AE64;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// System.Type
struct Type_t;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// Vars
struct Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3;
// ZoomInAnimation
struct ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431;
// ZoomOutAnimation
struct ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9;
// BallsPathfinder/<BallMovement>d__29
struct U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24;

IL2CPP_EXTERN_C RuntimeClass* GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral075488768B36FC6B56A29920D44014060332E6C1;
IL2CPP_EXTERN_C String_t* _stringLiteral2B4CB989CF705BDB31F2D19B8AB3AB7081C4C9DB;
IL2CPP_EXTERN_C String_t* _stringLiteral36282FAC116D9FD6B37CC425310E1A8510F08A53;
IL2CPP_EXTERN_C String_t* _stringLiteral4C0B37ABB062FCBA44BD6462905450F47F6D60E3;
IL2CPP_EXTERN_C String_t* _stringLiteral5B68E4FC12CF26A2805AD3EC3EDA6F38D0B99347;
IL2CPP_EXTERN_C String_t* _stringLiteral6271B5A613D8E9F509493567C383DD2671517B43;
IL2CPP_EXTERN_C String_t* _stringLiteral62CE1AE2C494852B7905BEEC1F46C7400044A2C7;
IL2CPP_EXTERN_C String_t* _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D;
IL2CPP_EXTERN_C String_t* _stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE;
IL2CPP_EXTERN_C String_t* _stringLiteralA5026BA636AED7E7171601C8E89BFD371EADA710;
IL2CPP_EXTERN_C String_t* _stringLiteralA74ACAA1F61DE0EB348EC03946685B0B6270CB36;
IL2CPP_EXTERN_C String_t* _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9;
IL2CPP_EXTERN_C String_t* _stringLiteralBFCAC1F6985AA9B53AA3AA1B94148E11F91CCAE4;
IL2CPP_EXTERN_C String_t* _stringLiteralCD078B3D013E9178455A33BB9E00BF4A1C4A52EE;
IL2CPP_EXTERN_C String_t* _stringLiteralD0DABD215A28AA39516A755A399D76F8E7E493C0;
IL2CPP_EXTERN_C String_t* _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE;
IL2CPP_EXTERN_C String_t* _stringLiteralE5C37D372367C69DCD30FE688631A1B0CE49EA73;
IL2CPP_EXTERN_C String_t* _stringLiteralED35FF3F063D29A026AE09F18392D9C7537823F2;
IL2CPP_EXTERN_C String_t* _stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431_mF70B3A761AB9683B1A957B7117C21A3FA5BDC439_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9_m73FE44109D7EC9A0AE531876384FF3B9E8FB1CB2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisBallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850_m8B28A0D7DB7CA11CC12E3400141E211AD7BBBBDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisBallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990_mEC272194416C4A4FAD035A00B789D99ABB9B83AE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisDestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4_m1C5F9FE0BF5F039826E0927112D338BE64D9764D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CBallMovementU3Ed__29_System_Collections_IEnumerator_Reset_m34433EBCFA168D9DDA8E486D0BADD873ACCAC264_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ImageU5BU5D_t8869694C217655DA7B1315DC02C80F1308B78B78;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
struct Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};
struct Il2CppArrayBounds;

// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// BallsPathfinder/<BallMovement>d__29
struct U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63  : public RuntimeObject
{
	// System.Int32 BallsPathfinder/<BallMovement>d__29::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object BallsPathfinder/<BallMovement>d__29::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// System.Int32[,] BallsPathfinder/<BallMovement>d__29::path
	Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___path_2;
	// System.Int32 BallsPathfinder/<BallMovement>d__29::xTarget
	int32_t ___xTarget_3;
	// System.Int32 BallsPathfinder/<BallMovement>d__29::yTarget
	int32_t ___yTarget_4;
	// System.Int32 BallsPathfinder/<BallMovement>d__29::ballColor
	int32_t ___ballColor_5;
	// BallsPathfinder BallsPathfinder/<BallMovement>d__29::<>4__this
	BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* ___U3CU3E4__this_6;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2  : public ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F
{
};

struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_StaticFields
{
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___enumSeperatorCharArray_0;
};
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2A1A94B24E3B776EEF4E5E485E290BB9D4D072E2_marshaled_com
{
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	float ___m_Seconds_0;
};

// System.Reflection.BindingFlags
struct BindingFlags_t5DC2835E4AE9C1862B3AD172EF35B6A5F4F1812C 
{
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B 
{
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;
};

// BallsPathfinder/Status
struct Status_t66D728F4056118EC5A87C88B749E2F6897986586 
{
	// System.Int32 BallsPathfinder/Status::value__
	int32_t ___value___2;
};

// UnityEngine.UI.Image/FillMethod
struct FillMethod_t36837ED12068DF1582CC20489D571B0BCAA7AD19 
{
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;
};

// UnityEngine.UI.Image/Type
struct Type_t81D6F138C2FC745112D5247CD91BD483EDFFC041 
{
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// System.Type
struct Type_t  : public MemberInfo_t
{
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ____impl_8;
};

struct Type_t_StaticFields
{
	// System.Reflection.Binder modreq(System.Runtime.CompilerServices.IsVolatile) System.Type::s_defaultBinder
	Binder_t91BFCE95A7057FADF4D8A1A342AFE52872246235* ___s_defaultBinder_0;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_1;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t97234E1129B564EB38B8D85CAC2AD8B5B9522FFB* ___EmptyTypes_2;
	// System.Object System.Type::Missing
	RuntimeObject* ___Missing_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_tF644F1AE82F611B677CE1964D5A3277DDA21D553* ___FilterNameIgnoreCase_6;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.AudioBehaviour
struct AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.Camera
struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

struct Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184_StaticFields
{
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t844E527BFE37BC0495E7F67993E43C07642DA9DD* ___onPostRender_6;
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5  : public Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1
{
};

struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_StaticFields
{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24* ___reapplyDrivenProperties_4;
};

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B  : public Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF
{
	// UnityEngine.Events.UnityEvent`1<UnityEngine.SpriteRenderer> UnityEngine.SpriteRenderer::m_SpriteChangeEvent
	UnityEvent_1_t8ABE5544759145B8D7A09F1C54FFCB6907EDD56E* ___m_SpriteChangeEvent_4;
};

// UnityEngine.AudioSource
struct AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299  : public AudioBehaviour_t2DC0BEF7B020C952F3D2DA5AAAC88501C7EEB941
{
};

// BallExplosion
struct BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject BallExplosion::explosionParticle
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___explosionParticle_4;
};

// BallsPathfinder
struct BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Int32 BallsPathfinder::numberOfRows
	int32_t ___numberOfRows_4;
	// System.Int32 BallsPathfinder::numberOfColumns
	int32_t ___numberOfColumns_5;
	// System.Int32 BallsPathfinder::iPath
	int32_t ___iPath_6;
	// UnityEngine.GameObject BallsPathfinder::tiles
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___tiles_7;
	// System.Int32 BallsPathfinder::numberOfBalls
	int32_t ___numberOfBalls_8;
	// System.Boolean BallsPathfinder::isFirstWave
	bool ___isFirstWave_9;
	// System.Int32[,] BallsPathfinder::placeholderBalls
	Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___placeholderBalls_10;
	// System.Int32 BallsPathfinder::lastScore
	int32_t ___lastScore_11;
	// System.Int32 BallsPathfinder::numberOfConsecutiveBalls
	int32_t ___numberOfConsecutiveBalls_12;
	// System.Int32 BallsPathfinder::currentCellValue
	int32_t ___currentCellValue_13;
	// System.Int32 BallsPathfinder::k1
	int32_t ___k1_14;
	// System.Int32 BallsPathfinder::k2
	int32_t ___k2_15;
	// System.Boolean BallsPathfinder::flag
	bool ___flag_16;
};

// DestroyExplosionParticle
struct DestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// Menus
struct Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject Menus::mainMenuUI
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___mainMenuUI_4;
	// UnityEngine.GameObject Menus::gameplayUI
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameplayUI_5;
	// UnityEngine.GameObject Menus::pauseMenuUI
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___pauseMenuUI_6;
	// UnityEngine.GameObject Menus::gameOverMenuUI
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameOverMenuUI_7;
	// UnityEngine.GameObject Menus::mainButtons
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___mainButtons_8;
	// BallsPathfinder Menus::pathFinder
	BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* ___pathFinder_9;
	// UnityEngine.AudioSource Menus::buttonSound
	AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* ___buttonSound_10;
	// UnityEngine.Sprite[] Menus::balls
	SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* ___balls_11;
	// UnityEngine.UI.Image[] Menus::nextWaveBalls
	ImageU5BU5D_t8869694C217655DA7B1315DC02C80F1308B78B78* ___nextWaveBalls_12;
	// UnityEngine.UI.Text Menus::score
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___score_13;
	// UnityEngine.UI.Text Menus::bestScore
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___bestScore_14;
	// UnityEngine.UI.Text Menus::timer
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___timer_15;
	// System.Single Menus::seconds
	float ___seconds_16;
	// System.Int32 Menus::minutes
	int32_t ___minutes_17;
	// System.Int32 Menus::hour
	int32_t ___hour_18;
};

// SelectedBallAnimation
struct SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean SelectedBallAnimation::up
	bool ___up_4;
	// System.Single SelectedBallAnimation::scale
	float ___scale_5;
};

// Tile
struct Tile_t192D2F5511792792FB74C37341AFAA9F6B77AE64  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// Vars
struct Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

struct Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields
{
	// System.Int32[,] Vars::fields
	Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___fields_4;
	// System.Int32 Vars::ballStartPosX
	int32_t ___ballStartPosX_5;
	// System.Int32 Vars::ballStartPosY
	int32_t ___ballStartPosY_6;
	// UnityEngine.GameObject Vars::ball
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ball_7;
	// System.Boolean Vars::isBallMoving
	bool ___isBallMoving_8;
	// System.Int32 Vars::score
	int32_t ___score_9;
};

// ZoomInAnimation
struct ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.RectTransform ZoomInAnimation::rectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___rectTransform_4;
	// System.Single ZoomInAnimation::scale
	float ___scale_5;
};

// ZoomOutAnimation
struct ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.RectTransform ZoomOutAnimation::rectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___rectTransform_4;
	// System.Single ZoomOutAnimation::scale
	float ___scale_5;
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;
};

struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_21;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_35;
};

// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;
};

struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tE6BB71ABF15905EFA2BE92C38A2716547AEADB19* ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;
};

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts_42;
};

struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText_40;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[,]
struct Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
	inline int32_t GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, int32_t value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t8869694C217655DA7B1315DC02C80F1308B78B78  : public RuntimeArray
{
	ALIGN_FIELD (8) Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* m_Items[1];

	inline Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Sprite[]
struct SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B  : public RuntimeArray
{
	ALIGN_FIELD (8) Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* m_Items[1];

	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;

// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51 (String_t* ___name0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<DestroyExplosionParticle>()
inline DestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4* GameObject_GetComponent_TisDestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4_m1C5F9FE0BF5F039826E0927112D338BE64D9764D (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  DestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A (Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::Initializefields(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_Initializefields_m7B5B1A05A8426BCFF9B25CA75AC578001260F627 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___rows0, int32_t ___columns1, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CreateTiles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreateTiles_mDB15E60160F8C59BFFE014A9A048DB9292D18D91 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CreateNewBalls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreateNewBalls_m39799563225761B4B7D54ECDD5BB1BCA9AAE76DD (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Int32 System.Array::GetLength(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935 (RuntimeArray* __this, int32_t ___dimension0, const RuntimeMethod* method) ;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t* Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E (RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B ___handle0, const RuntimeMethod* method) ;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* Resources_Load_mDCC8EBD3196F1CE1B86E74416AD90CF86320C401 (String_t* ___path0, Type_t* ___systemTypeInstance1, const RuntimeMethod* method) ;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* Object_Instantiate_m24741FECC461F230DBD3E76CD6AEE2E395CB340D (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___original0, const RuntimeMethod* method) ;
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5 (int32_t* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43 (const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8 (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C (const RuntimeMethod* method) ;
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Camera_set_orthographicSize_m76DD021032ACB3DDBD052B75EC66DCE3A7295A5C (Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* __this, float ___value0, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8 (int32_t ___minInclusive0, int32_t ___maxExclusive1, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CreatePlaceholderBalls(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreatePlaceholderBalls_mF9E3691BB672251EBF290FFA0C6C347C7799A269 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___numberOfBallsToCreate0, const RuntimeMethod* method) ;
// System.Int32 BallsPathfinder::CheckForAvailabeFields(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BallsPathfinder_CheckForAvailabeFields_m65E0E87FFECB82698062D1E6B1C0F6D9DA455CAB (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___numberOfBallsToCreate0, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::ConvertPlaceholderBallsToRealBalls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_ConvertPlaceholderBallsToRealBalls_mB70FFEBD19BFC154933C9556587A73E88D9D3099 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CheckScore()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckScore_m6345AE27461D331492D27D28E16945CCF09411F5 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
inline AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void UnityEngine.AudioSource::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3 (AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<Menus>()
inline Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Void Menus::GameOverMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_GameOverMenu_m55C974EC861F3C28CB8BAAF89883DF51C0D291F7 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, String_t* ___n0, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<BallExplosion>()
inline BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850* GameObject_GetComponent_TisBallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850_m8B28A0D7DB7CA11CC12E3400141E211AD7BBBBDA (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void BallExplosion::ActivateBallExplosion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallExplosion_ActivateBallExplosion_mA79BA98CFE9D22708F489CE5561504B2F9486DD5 (BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___value0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_sortingOrder_m4C67F002AD68CA0D55D20D6B78CDED3DB24467DA (Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* __this, int32_t ___value0, const RuntimeMethod* method) ;
// System.Void Menus::UpdateNextWaveBallsColor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_UpdateNextWaveBallsColor_mDDCDA9C3B75918F26B537B10C3C97DFC272BE1A5 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, int32_t ___ball0, int32_t ___color1, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::ResetConsecuteveBallsSearchingVariables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::DestoyBall(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method) ;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Max_m830F00B616D7A2130E46E974DFB27E9DA7FE30E5 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method) ;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method) ;
// System.Boolean BallsPathfinder::CheckForScoreDiagonallyTraverse(System.Int32[,],System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BallsPathfinder_CheckForScoreDiagonallyTraverse_m38B44ED6144820ABF5C94721B87FC4B475D84C7A (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___m0, int32_t ___i1, int32_t ___j2, int32_t ___row3, int32_t ___col4, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CheckForScoreVertically()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckForScoreVertically_m77901B14D506A171956F78185326656C51FB0785 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CheckForScoreHorizontally()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckForScoreHorizontally_m079A65463A19BB59EA49B8C9530061206A2BFAC1 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CheckForScoreDiagonally()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckForScoreDiagonally_m0D8C80E75B167112F06671DDE6175BF698820B80 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Void Menus::UpdateScore()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_UpdateScore_m6D7C68AE360246A76F901FE59837276BF601E8DC (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) ;
// System.Int32[,] BallsPathfinder::FindPath(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* BallsPathfinder_FindPath_m7FA847FADB1462CBBB01437FE18C89FFD28B5597 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___iFromY0, int32_t ___iFromX1, int32_t ___iToY2, int32_t ___iToX3, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<SelectedBallAnimation>()
inline SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Collections.IEnumerator BallsPathfinder::BallMovement(System.Int32[,],System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* BallsPathfinder_BallMovement_m48DD0BF5B50C07413E4D143C31463126971EEB49 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___path0, int32_t ___xTarget1, int32_t ___yTarget2, int32_t ___ballColor3, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Void BallsPathfinder/<BallMovement>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBallMovementU3Ed__29__ctor_mE3D83FBDE142D6E79AC3714B748B82A3379152B1 (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Int32 BallsPathfinder::get_Cols()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BallsPathfinder_get_Cols_m2D237BCDE09D2ED862D8855D71B86BDBCDE01C14_inline (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) ;
// System.Int32[,] BallsPathfinder::Search(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* BallsPathfinder_Search_m29FA4638DD4989B518626DC247A9CCCE00CD7F72 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___iStart0, int32_t ___iStop1, const RuntimeMethod* method) ;
// System.Int32 BallsPathfinder::GetNodeContents(System.Int32[,],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___iMaze0, int32_t ___iNodeNo1, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::ChangeNodeContents(System.Int32[,],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___iMaze0, int32_t ___iNodeNo1, int32_t ___iNewValue2, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* __this, float ___seconds0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mCF3935E28AC7B30B279F07F9321CC56718E1311A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___x0, Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___y1, const RuntimeMethod* method) ;
// System.Void BallsPathfinder::CreateNewBall(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreateNewBall_m29425C07E0D9CE01502985D39A7ED770E2D94279 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___placeholderXPos0, int32_t ___placeholderYPos1, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m0E1B4CF8C29EB7FC8658C2C84C57F49C0DD12C91 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, float ___t1, const RuntimeMethod* method) ;
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D (const RuntimeMethod* method) ;
// System.String System.Int32::ToString(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m967AECC237535C552A97A80C7875E31B98496CA9 (int32_t* __this, String_t* ___format0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___values0, const RuntimeMethod* method) ;
// System.Void Menus::UpdateTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_UpdateTimer_mB8AD879D0B4372CD0C5F32370D09D81096481148 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PlayerPrefs_GetInt_m35C13A87BBC7907554CE5405EB5D00CF85E7457B (String_t* ___key0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Application::Quit()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Application_Quit_m965C6D4CA85A24DD95B347D22837074F19C58134 (const RuntimeMethod* method) ;
// System.Void Menus::DestroyAllTiles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_DestroyAllTiles_m1D62BB3B9F661210BF1800BC086A4E4AC5558891 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) ;
// System.Void Vars::ResetAllVariables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vars_ResetAllVariables_m038B64E49DA06DAC269D70A416CD22A1B6A80D9D (const RuntimeMethod* method) ;
// System.Void Menus::ResetTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_ResetTimer_m5A959F0FF4A0C9A61F36E21DB58E70EA725D0D18 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_Invoke_mF724350C59362B0F1BFE26383209A274A29A63FB (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, String_t* ___methodName0, float ___time1, const RuntimeMethod* method) ;
// System.Void Menus::StartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_StartGame_m2B84735B2B5A9DA804212E62FBFF2E9FC7F69B21 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) ;
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_mC0C248340BA27AAEE56855A3FAFA0D8CA12956DE (Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* __this, Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerPrefs_SetInt_mDC9617BFD56FEC670626A1002D9A5FE963D8D175 (String_t* ___key0, int32_t ___value1, const RuntimeMethod* method) ;
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392 (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* __this, const RuntimeMethod* method) ;
// System.Int32 System.String::IndexOf(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_IndexOf_m69E9BDAFD93767C85A7FF861B453415D3B4A200F (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) ;
// System.Int32 System.String::LastIndexOf(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_LastIndexOf_m8923DBD89F2B3E5A34190B038B48F402E0C17E40 (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.String System.String::Substring(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE (String_t* __this, int32_t ___startIndex0, int32_t ___length1, const RuntimeMethod* method) ;
// System.Int32 System.Int32::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767 (String_t* ___s0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<BallsPathfinder>()
inline BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* GameObject_GetComponent_TisBallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990_mEC272194416C4A4FAD035A00B789D99ABB9B83AE (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void BallsPathfinder::InitializeBallMovement(UnityEngine.GameObject,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_InitializeBallMovement_m9E10C61F99868F73E615449DF46689DD33CD340B (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ball0, int32_t ___xTarget1, int32_t ___yTarget2, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<ZoomInAnimation>()
inline ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431* Component_GetComponent_TisZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431_mF70B3A761AB9683B1A957B7117C21A3FA5BDC439 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// T UnityEngine.Component::GetComponent<ZoomOutAnimation>()
inline ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9* Component_GetComponent_TisZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9_m73FE44109D7EC9A0AE531876384FF3B9E8FB1CB2 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m5447BF12C18339431AB8AF02FA463C543D88D463_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BallExplosion::ActivateBallExplosion()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallExplosion_ActivateBallExplosion_mA79BA98CFE9D22708F489CE5561504B2F9486DD5 (BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisDestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4_m1C5F9FE0BF5F039826E0927112D338BE64D9764D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD0DABD215A28AA39516A755A399D76F8E7E493C0);
		s_Il2CppMethodInitialized = true;
	}
	{
		// explosionParticle.transform.parent = GameObject.Find("Tiles").transform;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___explosionParticle_4;
		NullCheck(L_0);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_0, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralD0DABD215A28AA39516A755A399D76F8E7E493C0, NULL);
		NullCheck(L_2);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_2, NULL);
		NullCheck(L_1);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_1, L_3, NULL);
		// explosionParticle.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = __this->___explosionParticle_4;
		NullCheck(L_4);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_4, (bool)1, NULL);
		// explosionParticle.GetComponent<DestroyExplosionParticle> ().enabled = true;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = __this->___explosionParticle_4;
		NullCheck(L_5);
		DestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4* L_6;
		L_6 = GameObject_GetComponent_TisDestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4_m1C5F9FE0BF5F039826E0927112D338BE64D9764D(L_5, GameObject_GetComponent_TisDestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4_m1C5F9FE0BF5F039826E0927112D338BE64D9764D_RuntimeMethod_var);
		NullCheck(L_6);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_6, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void BallExplosion::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallExplosion__ctor_mB0CC125BBE27181A04CA95DF3D6E0819E3703FD4 (BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BallsPathfinder::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_OnEnable_m23A5C290F0765720298791359A18F5BB6BA5049E (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD0DABD215A28AA39516A755A399D76F8E7E493C0);
		s_Il2CppMethodInitialized = true;
	}
	{
		// isFirstWave = true;
		__this->___isFirstWave_9 = (bool)1;
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		// placeholderBalls = null;
		__this->___placeholderBalls_10 = (Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)NULL;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___placeholderBalls_10), (void*)(Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)NULL);
		// lastScore = 0;
		__this->___lastScore_11 = 0;
		// currentCellValue = 0;
		__this->___currentCellValue_13 = 0;
		// k1 = 0;
		__this->___k1_14 = 0;
		// k2 = 0;
		__this->___k2_15 = 0;
		// flag = true;
		__this->___flag_16 = (bool)1;
		// Initializefields(numberOfRows, numberOfColumns);
		int32_t L_0 = __this->___numberOfRows_4;
		int32_t L_1 = __this->___numberOfColumns_5;
		BallsPathfinder_Initializefields_m7B5B1A05A8426BCFF9B25CA75AC578001260F627(__this, L_0, L_1, NULL);
		// tiles = GameObject.Find("Tiles");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralD0DABD215A28AA39516A755A399D76F8E7E493C0, NULL);
		__this->___tiles_7 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___tiles_7), (void*)L_2);
		// CreateTiles();
		BallsPathfinder_CreateTiles_mDB15E60160F8C59BFFE014A9A048DB9292D18D91(__this, NULL);
		// CreateNewBalls();
		BallsPathfinder_CreateNewBalls_m39799563225761B4B7D54ECDD5BB1BCA9AAE76DD(__this, NULL);
		// }
		return;
	}
}
// System.Void BallsPathfinder::Initializefields(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_Initializefields_m7B5B1A05A8426BCFF9B25CA75AC578001260F627 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___rows0, int32_t ___columns1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// Vars.fields = new int[rows, columns];
		int32_t L_0 = ___rows0;
		int32_t L_1 = ___columns1;
		il2cpp_array_size_t L_3[] = { (il2cpp_array_size_t)L_0, (il2cpp_array_size_t)L_1 };
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_2 = (Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)GenArrayNew(Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var, L_3);
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4), (void*)L_2);
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		V_0 = 0;
		goto IL_0037;
	}

IL_0010:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		V_1 = 0;
		goto IL_0025;
	}

IL_0014:
	{
		// Vars.fields[i, j] = 0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_4 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_4);
		(L_4)->SetAt(L_5, L_6, 0);
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_7 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_7, 1));
	}

IL_0025:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_8 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_9 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_9);
		int32_t L_10;
		L_10 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_9, 1, NULL);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_11, 1));
	}

IL_0037:
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_12 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_13 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_13);
		int32_t L_14;
		L_14 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_13, 0, NULL);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0010;
		}
	}
	{
		// }
		return;
	}
}
// System.Void BallsPathfinder::CreateTiles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreateTiles_mDB15E60160F8C59BFFE014A9A048DB9292D18D91 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		V_1 = 0;
		goto IL_0093;
	}

IL_0007:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		V_2 = 0;
		goto IL_007e;
	}

IL_000b:
	{
		// GameObject tile = Instantiate(Resources.Load("Tile", typeof(GameObject))) as GameObject;
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_0 = { reinterpret_cast<intptr_t> (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_1;
		L_1 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_0, NULL);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_2;
		L_2 = Resources_Load_mDCC8EBD3196F1CE1B86E74416AD90CF86320C401(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_1, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_3;
		L_3 = Object_Instantiate_m24741FECC461F230DBD3E76CD6AEE2E395CB340D(L_2, NULL);
		// tile.name = "Tile" + i + "X" + j;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ((GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)IsInstSealed((RuntimeObject*)L_3, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var));
		String_t* L_5;
		L_5 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_1), NULL);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_2), NULL);
		String_t* L_7;
		L_7 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_5, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_6, NULL);
		NullCheck(L_4);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_4, L_7, NULL);
		// tile.transform.position = new Vector2(i, j);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8 = L_4;
		NullCheck(L_8);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_9;
		L_9 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_8, NULL);
		int32_t L_10 = V_1;
		int32_t L_11 = V_2;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_12), ((float)L_10), ((float)L_11), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_12, NULL);
		NullCheck(L_9);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_9, L_13, NULL);
		// tile.transform.parent = tiles.transform;
		NullCheck(L_8);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_14;
		L_14 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_8, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15 = __this->___tiles_7;
		NullCheck(L_15);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_16;
		L_16 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_15, NULL);
		NullCheck(L_14);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_14, L_16, NULL);
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_17 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_17, 1));
	}

IL_007e:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_18 = V_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_19 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_19);
		int32_t L_20;
		L_20 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_19, 1, NULL);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_000b;
		}
	}
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_21, 1));
	}

IL_0093:
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_22 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_23 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_23);
		int32_t L_24;
		L_24 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_23, 0, NULL);
		if ((((int32_t)L_22) < ((int32_t)L_24)))
		{
			goto IL_0007;
		}
	}
	{
		// Camera.main.transform.position = new Vector3(tiles.transform.position.x + Vars.fields.GetLength(0) / 2, (tiles.transform.position.y + Vars.fields.GetLength(1) / 2), -10);
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_25;
		L_25 = Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43(NULL);
		NullCheck(L_25);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_26;
		L_26 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_25, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27 = __this->___tiles_7;
		NullCheck(L_27);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_28;
		L_28 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_27, NULL);
		NullCheck(L_28);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_29;
		L_29 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_28, NULL);
		float L_30 = L_29.___x_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_31 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_31);
		int32_t L_32;
		L_32 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_31, 0, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_33 = __this->___tiles_7;
		NullCheck(L_33);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_34;
		L_34 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_33, NULL);
		NullCheck(L_34);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_35;
		L_35 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_34, NULL);
		float L_36 = L_35.___y_3;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_37 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_37);
		int32_t L_38;
		L_38 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_37, 1, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_39;
		memset((&L_39), 0, sizeof(L_39));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_39), ((float)il2cpp_codegen_add(L_30, ((float)((int32_t)(L_32/2))))), ((float)il2cpp_codegen_add(L_36, ((float)((int32_t)(L_38/2))))), (-10.0f), /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_26, L_39, NULL);
		// float screenAspectRatio = (float)Screen.height / (float)Screen.width;
		int32_t L_40;
		L_40 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		int32_t L_41;
		L_41 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		V_0 = ((float)(((float)L_40)/((float)L_41)));
		// Camera.main.orthographicSize = (numberOfColumns / 2) * screenAspectRatio + 1;
		Camera_tA92CC927D7439999BC82DBEDC0AA45B470F9E184* L_42;
		L_42 = Camera_get_main_mF222B707D3BF8CC9C7544609EFC71CFB62E81D43(NULL);
		int32_t L_43 = __this->___numberOfColumns_5;
		float L_44 = V_0;
		NullCheck(L_42);
		Camera_set_orthographicSize_m76DD021032ACB3DDBD052B75EC66DCE3A7295A5C(L_42, ((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(((float)((int32_t)(L_43/2))), L_44)), (1.0f))), NULL);
		// }
		return;
	}
}
// System.Void BallsPathfinder::CreateNewBalls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreateNewBalls_m39799563225761B4B7D54ECDD5BB1BCA9AAE76DD (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6271B5A613D8E9F509493567C383DD2671517B43);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// if (isFirstWave)
		bool L_0 = __this->___isFirstWave_9;
		if (!L_0)
		{
			goto IL_00fc;
		}
	}
	{
		// numberOfBallsToCreate = 5;//This many balls will be created on the first wave
		V_0 = 5;
		// isFirstWave = false;
		__this->___isFirstWave_9 = (bool)0;
		goto IL_00ee;
	}

IL_0019:
	{
		// int ballXPos = UnityEngine.Random.Range(0, numberOfRows);
		int32_t L_1 = __this->___numberOfRows_4;
		int32_t L_2;
		L_2 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_1, NULL);
		V_1 = L_2;
		// int ballYPos = UnityEngine.Random.Range(0, numberOfColumns);
		int32_t L_3 = __this->___numberOfColumns_5;
		int32_t L_4;
		L_4 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_3, NULL);
		V_2 = L_4;
		// if (Vars.fields[ballXPos, ballYPos] != 0) continue;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_5 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_6 = V_1;
		int32_t L_7 = V_2;
		NullCheck(L_5);
		int32_t L_8;
		L_8 = (L_5)->GetAt(L_6, L_7);
		if (L_8)
		{
			goto IL_00ee;
		}
	}
	{
		// int ballColor = UnityEngine.Random.Range(0, numberOfBalls);
		int32_t L_9 = __this->___numberOfBalls_8;
		int32_t L_10;
		L_10 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_9, NULL);
		V_3 = L_10;
		// Vars.fields[ballXPos, ballYPos] = (ballColor + 1);
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_11 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		int32_t L_14 = V_3;
		NullCheck(L_11);
		(L_11)->SetAt(L_12, L_13, ((int32_t)il2cpp_codegen_add(L_14, 1)));
		// GameObject ball = Instantiate(Resources.Load("Ball" + (ballColor + 1), typeof(GameObject))) as GameObject;
		int32_t L_15 = V_3;
		V_4 = ((int32_t)il2cpp_codegen_add(L_15, 1));
		String_t* L_16;
		L_16 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_4), NULL);
		String_t* L_17;
		L_17 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, L_16, NULL);
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_18 = { reinterpret_cast<intptr_t> (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_19;
		L_19 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_18, NULL);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_20;
		L_20 = Resources_Load_mDCC8EBD3196F1CE1B86E74416AD90CF86320C401(L_17, L_19, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_21;
		L_21 = Object_Instantiate_m24741FECC461F230DBD3E76CD6AEE2E395CB340D(L_20, NULL);
		// ball.name = "Ball";
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = ((GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)IsInstSealed((RuntimeObject*)L_21, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var));
		NullCheck(L_22);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_22, _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, NULL);
		// ball.transform.parent = GameObject.Find("Tile" + ballXPos + "X" + ballYPos).transform;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23 = L_22;
		NullCheck(L_23);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_24;
		L_24 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_23, NULL);
		String_t* L_25;
		L_25 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_1), NULL);
		String_t* L_26;
		L_26 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_2), NULL);
		String_t* L_27;
		L_27 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_25, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_26, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_27, NULL);
		NullCheck(L_28);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_29;
		L_29 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_28, NULL);
		NullCheck(L_24);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_24, L_29, NULL);
		// ball.transform.localPosition = new Vector2(0, 0);
		NullCheck(L_23);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_30;
		L_30 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_23, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_31;
		memset((&L_31), 0, sizeof(L_31));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_31), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
		L_32 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_31, NULL);
		NullCheck(L_30);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_30, L_32, NULL);
		// numberOfBallsToCreate--;
		int32_t L_33 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract(L_33, 1));
	}

IL_00ee:
	{
		// while (numberOfBallsToCreate != 0)
		int32_t L_34 = V_0;
		if (L_34)
		{
			goto IL_0019;
		}
	}
	{
		// CreatePlaceholderBalls(3);
		BallsPathfinder_CreatePlaceholderBalls_mF9E3691BB672251EBF290FFA0C6C347C7799A269(__this, 3, NULL);
		return;
	}

IL_00fc:
	{
		// numberOfBallsToCreate = 3;
		V_0 = 3;
		// CheckForAvailabeFields(numberOfBallsToCreate);
		int32_t L_35 = V_0;
		int32_t L_36;
		L_36 = BallsPathfinder_CheckForAvailabeFields_m65E0E87FFECB82698062D1E6B1C0F6D9DA455CAB(__this, L_35, NULL);
		// ConvertPlaceholderBallsToRealBalls();
		BallsPathfinder_ConvertPlaceholderBallsToRealBalls_mB70FFEBD19BFC154933C9556587A73E88D9D3099(__this, NULL);
		// CheckScore();
		BallsPathfinder_CheckScore_m6345AE27461D331492D27D28E16945CCF09411F5(__this, NULL);
		// CreatePlaceholderBalls(numberOfBallsToCreate);
		int32_t L_37 = V_0;
		BallsPathfinder_CreatePlaceholderBalls_mF9E3691BB672251EBF290FFA0C6C347C7799A269(__this, L_37, NULL);
		// if (lastScore != Vars.score)
		int32_t L_38 = __this->___lastScore_11;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_39 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		if ((((int32_t)L_38) == ((int32_t)L_39)))
		{
			goto IL_0145;
		}
	}
	{
		// lastScore = Vars.score;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_40 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		__this->___lastScore_11 = L_40;
		// GameObject.Find("BallPopSound").GetComponent<AudioSource>().Play();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_41;
		L_41 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral6271B5A613D8E9F509493567C383DD2671517B43, NULL);
		NullCheck(L_41);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_42;
		L_42 = GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A(L_41, GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		NullCheck(L_42);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_42, NULL);
	}

IL_0145:
	{
		// }
		return;
	}
}
// System.Void BallsPathfinder::CreateNewBall(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreateNewBall_m29425C07E0D9CE01502985D39A7ED770E2D94279 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___placeholderXPos0, int32_t ___placeholderYPos1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// int numberOfBallsToCreate = 1;
		V_0 = 1;
		goto IL_00e4;
	}

IL_0007:
	{
		// int ballXPos = UnityEngine.Random.Range(0, numberOfRows);
		int32_t L_0 = __this->___numberOfRows_4;
		int32_t L_1;
		L_1 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_0, NULL);
		V_1 = L_1;
		// int ballYPos = UnityEngine.Random.Range(0, numberOfColumns);
		int32_t L_2 = __this->___numberOfColumns_5;
		int32_t L_3;
		L_3 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_2, NULL);
		V_2 = L_3;
		// if (Vars.fields[ballXPos, ballYPos] != 0) continue;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_4 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_4);
		int32_t L_7;
		L_7 = (L_4)->GetAt(L_5, L_6);
		if (L_7)
		{
			goto IL_00e4;
		}
	}
	{
		// int ballColor = placeholderBalls[placeholderXPos, placeholderYPos];
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_8 = __this->___placeholderBalls_10;
		int32_t L_9 = ___placeholderXPos0;
		int32_t L_10 = ___placeholderYPos1;
		NullCheck(L_8);
		int32_t L_11;
		L_11 = (L_8)->GetAt(L_9, L_10);
		V_3 = L_11;
		// placeholderBalls[placeholderXPos, placeholderYPos] = 0;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_12 = __this->___placeholderBalls_10;
		int32_t L_13 = ___placeholderXPos0;
		int32_t L_14 = ___placeholderYPos1;
		NullCheck(L_12);
		(L_12)->SetAt(L_13, L_14, 0);
		// Vars.fields[ballXPos, ballYPos] = ballColor;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_15 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_16 = V_1;
		int32_t L_17 = V_2;
		int32_t L_18 = V_3;
		NullCheck(L_15);
		(L_15)->SetAt(L_16, L_17, L_18);
		// GameObject ball = Instantiate(Resources.Load("Ball" + ballColor, typeof(GameObject))) as GameObject;
		String_t* L_19;
		L_19 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_3), NULL);
		String_t* L_20;
		L_20 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, L_19, NULL);
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_21 = { reinterpret_cast<intptr_t> (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_22;
		L_22 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_21, NULL);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_23;
		L_23 = Resources_Load_mDCC8EBD3196F1CE1B86E74416AD90CF86320C401(L_20, L_22, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_24;
		L_24 = Object_Instantiate_m24741FECC461F230DBD3E76CD6AEE2E395CB340D(L_23, NULL);
		// ball.name = "Ball";
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_25 = ((GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)IsInstSealed((RuntimeObject*)L_24, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var));
		NullCheck(L_25);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_25, _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, NULL);
		// ball.transform.parent = GameObject.Find("Tile" + ballXPos + "X" + ballYPos).transform;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26 = L_25;
		NullCheck(L_26);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_27;
		L_27 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_26, NULL);
		String_t* L_28;
		L_28 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_1), NULL);
		String_t* L_29;
		L_29 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_2), NULL);
		String_t* L_30;
		L_30 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_28, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_29, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_31;
		L_31 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_30, NULL);
		NullCheck(L_31);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_32;
		L_32 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_31, NULL);
		NullCheck(L_27);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_27, L_32, NULL);
		// ball.transform.localPosition = new Vector2(0, 0);
		NullCheck(L_26);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_33;
		L_33 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_26, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_34;
		memset((&L_34), 0, sizeof(L_34));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_34), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_35;
		L_35 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_34, NULL);
		NullCheck(L_33);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_33, L_35, NULL);
		// numberOfBallsToCreate--;
		int32_t L_36 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_subtract(L_36, 1));
	}

IL_00e4:
	{
		// while (numberOfBallsToCreate != 0)
		int32_t L_37 = V_0;
		if (L_37)
		{
			goto IL_0007;
		}
	}
	{
		// CheckScore();
		BallsPathfinder_CheckScore_m6345AE27461D331492D27D28E16945CCF09411F5(__this, NULL);
		// if (lastScore != Vars.score)
		int32_t L_38 = __this->___lastScore_11;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_39 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		if ((((int32_t)L_38) == ((int32_t)L_39)))
		{
			goto IL_0108;
		}
	}
	{
		// lastScore = Vars.score;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_40 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		__this->___lastScore_11 = L_40;
	}

IL_0108:
	{
		// }
		return;
	}
}
// System.Int32 BallsPathfinder::CheckForAvailabeFields(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BallsPathfinder_CheckForAvailabeFields_m65E0E87FFECB82698062D1E6B1C0F6D9DA455CAB (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___numberOfBallsToCreate0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4C0B37ABB062FCBA44BD6462905450F47F6D60E3);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// int numberOfEmptyFields = 0;
		V_0 = 0;
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		V_1 = 0;
		goto IL_0032;
	}

IL_0006:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		V_2 = 0;
		goto IL_0020;
	}

IL_000a:
	{
		// if (Vars.fields[i, j] == 0)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_0);
		int32_t L_3;
		L_3 = (L_0)->GetAt(L_1, L_2);
		if (L_3)
		{
			goto IL_001c;
		}
	}
	{
		// numberOfEmptyFields++;
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_4, 1));
	}

IL_001c:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_5 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
	}

IL_0020:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_6 = V_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_7 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_7);
		int32_t L_8;
		L_8 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_7, 1, NULL);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_000a;
		}
	}
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_9 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_9, 1));
	}

IL_0032:
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_10 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_11 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_11);
		int32_t L_12;
		L_12 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_11, 0, NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0006;
		}
	}
	{
		// if (numberOfEmptyFields == 0)
		int32_t L_13 = V_0;
		if (L_13)
		{
			goto IL_0064;
		}
	}
	{
		// GetComponent<Menus>().GameOverMenu();
		Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* L_14;
		L_14 = Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5(__this, Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5_RuntimeMethod_var);
		NullCheck(L_14);
		Menus_GameOverMenu_m55C974EC861F3C28CB8BAAF89883DF51C0D291F7(L_14, NULL);
		// GameObject.Find("GameOverSound").GetComponent<AudioSource>().Play();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15;
		L_15 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral4C0B37ABB062FCBA44BD6462905450F47F6D60E3, NULL);
		NullCheck(L_15);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_16;
		L_16 = GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A(L_15, GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		NullCheck(L_16);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_16, NULL);
		// return 0;
		return 0;
	}

IL_0064:
	{
		// return numberOfEmptyFields;
		int32_t L_17 = V_0;
		return L_17;
	}
}
// System.Void BallsPathfinder::DestoyBall(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___x0, int32_t ___y1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850_m8B28A0D7DB7CA11CC12E3400141E211AD7BBBBDA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameObject ball = GameObject.Find("Tile" + x + "X" + y).transform.Find("Ball").gameObject;
		String_t* L_0;
		L_0 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&___x0), NULL);
		String_t* L_1;
		L_1 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&___y1), NULL);
		String_t* L_2;
		L_2 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_0, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_1, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3;
		L_3 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_2, NULL);
		NullCheck(L_3);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_3, NULL);
		NullCheck(L_4);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5;
		L_5 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_4, _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, NULL);
		NullCheck(L_5);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_5, NULL);
		// ball.GetComponent<BallExplosion>().ActivateBallExplosion();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = L_6;
		NullCheck(L_7);
		BallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850* L_8;
		L_8 = GameObject_GetComponent_TisBallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850_m8B28A0D7DB7CA11CC12E3400141E211AD7BBBBDA(L_7, GameObject_GetComponent_TisBallExplosion_t4B371BEF9D3A8606A9AF08B227FE5BD3187CF850_m8B28A0D7DB7CA11CC12E3400141E211AD7BBBBDA_RuntimeMethod_var);
		NullCheck(L_8);
		BallExplosion_ActivateBallExplosion_mA79BA98CFE9D22708F489CE5561504B2F9486DD5(L_8, NULL);
		// Destroy(ball);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_7, NULL);
		// }
		return;
	}
}
// System.Void BallsPathfinder::CreatePlaceholderBalls(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CreatePlaceholderBalls_mF9E3691BB672251EBF290FFA0C6C347C7799A269 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___numberOfBallsToCreate0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// int availableFields = CheckForAvailabeFields(numberOfBallsToCreate);
		int32_t L_0 = ___numberOfBallsToCreate0;
		int32_t L_1;
		L_1 = BallsPathfinder_CheckForAvailabeFields_m65E0E87FFECB82698062D1E6B1C0F6D9DA455CAB(__this, L_0, NULL);
		V_0 = L_1;
		// if (numberOfBallsToCreate > availableFields)
		int32_t L_2 = ___numberOfBallsToCreate0;
		int32_t L_3 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)L_3)))
		{
			goto IL_000f;
		}
	}
	{
		// numberOfBallsToCreate = availableFields;
		int32_t L_4 = V_0;
		___numberOfBallsToCreate0 = L_4;
	}

IL_000f:
	{
		// placeholderBalls = new int[Vars.fields.GetLength(0), Vars.fields.GetLength(1)];
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_5 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_5);
		int32_t L_6;
		L_6 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_5, 0, NULL);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_7 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_7);
		int32_t L_8;
		L_8 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_7, 1, NULL);
		il2cpp_array_size_t L_10[] = { (il2cpp_array_size_t)L_6, (il2cpp_array_size_t)L_8 };
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_9 = (Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)GenArrayNew(Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var, L_10);
		__this->___placeholderBalls_10 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___placeholderBalls_10), (void*)L_9);
		goto IL_0156;
	}

IL_0035:
	{
		// int ballXPos = UnityEngine.Random.Range(0, numberOfRows);
		int32_t L_11 = __this->___numberOfRows_4;
		int32_t L_12;
		L_12 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_11, NULL);
		V_1 = L_12;
		// int ballYPos = UnityEngine.Random.Range(0, numberOfColumns);
		int32_t L_13 = __this->___numberOfColumns_5;
		int32_t L_14;
		L_14 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_13, NULL);
		V_2 = L_14;
		// if (Vars.fields[ballXPos, ballYPos] != 0 || placeholderBalls[ballXPos, ballYPos] != 0) continue;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_15 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_16 = V_1;
		int32_t L_17 = V_2;
		NullCheck(L_15);
		int32_t L_18;
		L_18 = (L_15)->GetAt(L_16, L_17);
		if (L_18)
		{
			goto IL_0156;
		}
	}
	{
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_19 = __this->___placeholderBalls_10;
		int32_t L_20 = V_1;
		int32_t L_21 = V_2;
		NullCheck(L_19);
		int32_t L_22;
		L_22 = (L_19)->GetAt(L_20, L_21);
		if (L_22)
		{
			goto IL_0156;
		}
	}
	{
		// int ballColor = UnityEngine.Random.Range(0, numberOfBalls);
		int32_t L_23 = __this->___numberOfBalls_8;
		int32_t L_24;
		L_24 = Random_Range_mD4D2DEE3D2E75D07740C9A6F93B3088B03BBB8F8(0, L_23, NULL);
		V_3 = L_24;
		// placeholderBalls[ballXPos, ballYPos] = (ballColor + 1);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_25 = __this->___placeholderBalls_10;
		int32_t L_26 = V_1;
		int32_t L_27 = V_2;
		int32_t L_28 = V_3;
		NullCheck(L_25);
		(L_25)->SetAt(L_26, L_27, ((int32_t)il2cpp_codegen_add(L_28, 1)));
		// GameObject ball = Instantiate(Resources.Load("Ball" + (ballColor + 1), typeof(GameObject))) as GameObject;
		int32_t L_29 = V_3;
		V_4 = ((int32_t)il2cpp_codegen_add(L_29, 1));
		String_t* L_30;
		L_30 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_4), NULL);
		String_t* L_31;
		L_31 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, L_30, NULL);
		RuntimeTypeHandle_t332A452B8B6179E4469B69525D0FE82A88030F7B L_32 = { reinterpret_cast<intptr_t> (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_0_0_0_var) };
		il2cpp_codegen_runtime_class_init_inline(Type_t_il2cpp_TypeInfo_var);
		Type_t* L_33;
		L_33 = Type_GetTypeFromHandle_m2570A2A5B32A5E9D9F0F38B37459DA18736C823E(L_32, NULL);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_34;
		L_34 = Resources_Load_mDCC8EBD3196F1CE1B86E74416AD90CF86320C401(L_31, L_33, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_35;
		L_35 = Object_Instantiate_m24741FECC461F230DBD3E76CD6AEE2E395CB340D(L_34, NULL);
		// ball.name = "BallPlaceholder";
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_36 = ((GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*)IsInstSealed((RuntimeObject*)L_35, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F_il2cpp_TypeInfo_var));
		NullCheck(L_36);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_36, _stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0, NULL);
		// ball.transform.localScale = new Vector2(0.5f, 0.5f);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_37 = L_36;
		NullCheck(L_37);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_38;
		L_38 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_37, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_39;
		memset((&L_39), 0, sizeof(L_39));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_39), (0.5f), (0.5f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_40;
		L_40 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_39, NULL);
		NullCheck(L_38);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_38, L_40, NULL);
		// ball.transform.parent = GameObject.Find("Tile" + ballXPos + "X" + ballYPos).transform;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_41 = L_37;
		NullCheck(L_41);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_42;
		L_42 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_41, NULL);
		String_t* L_43;
		L_43 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_1), NULL);
		String_t* L_44;
		L_44 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_2), NULL);
		String_t* L_45;
		L_45 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_43, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_44, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_46;
		L_46 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_45, NULL);
		NullCheck(L_46);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_47;
		L_47 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_46, NULL);
		NullCheck(L_42);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_42, L_47, NULL);
		// ball.transform.localPosition = new Vector2(0, 0);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_48 = L_41;
		NullCheck(L_48);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_49;
		L_49 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_48, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_50;
		memset((&L_50), 0, sizeof(L_50));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_50), (0.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_51;
		L_51 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_50, NULL);
		NullCheck(L_49);
		Transform_set_localPosition_mDE1C997F7D79C0885210B7732B4BA50EE7D73134(L_49, L_51, NULL);
		// ball.GetComponent<SpriteRenderer>().sortingOrder = -1;
		NullCheck(L_48);
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_52;
		L_52 = GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9(L_48, GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var);
		NullCheck(L_52);
		Renderer_set_sortingOrder_m4C67F002AD68CA0D55D20D6B78CDED3DB24467DA(L_52, (-1), NULL);
		// numberOfBallsToCreate--;
		int32_t L_53 = ___numberOfBallsToCreate0;
		___numberOfBallsToCreate0 = ((int32_t)il2cpp_codegen_subtract(L_53, 1));
		// GetComponent<Menus>().UpdateNextWaveBallsColor(numberOfBallsToCreate, ballColor);
		Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* L_54;
		L_54 = Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5(__this, Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5_RuntimeMethod_var);
		int32_t L_55 = ___numberOfBallsToCreate0;
		int32_t L_56 = V_3;
		NullCheck(L_54);
		Menus_UpdateNextWaveBallsColor_mDDCDA9C3B75918F26B537B10C3C97DFC272BE1A5(L_54, L_55, L_56, NULL);
	}

IL_0156:
	{
		// while (numberOfBallsToCreate != 0)
		int32_t L_57 = ___numberOfBallsToCreate0;
		if (L_57)
		{
			goto IL_0035;
		}
	}
	{
		// }
		return;
	}
}
// System.Void BallsPathfinder::ConvertPlaceholderBallsToRealBalls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_ConvertPlaceholderBallsToRealBalls_mB70FFEBD19BFC154933C9556587A73E88D9D3099 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// if (placeholderBalls == null) return;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = __this->___placeholderBalls_10;
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (placeholderBalls == null) return;
		return;
	}

IL_0009:
	{
		// for (int i = 0; i < placeholderBalls.GetLength(0); i++)
		V_0 = 0;
		goto IL_010e;
	}

IL_0010:
	{
		// for (int j = 0; j < placeholderBalls.GetLength(1); j++)
		V_1 = 0;
		goto IL_00f8;
	}

IL_0017:
	{
		// if (placeholderBalls[i, j] != 0)
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_1 = __this->___placeholderBalls_10;
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_1);
		int32_t L_4;
		L_4 = (L_1)->GetAt(L_2, L_3);
		if (!L_4)
		{
			goto IL_00f4;
		}
	}
	{
		// if (GameObject.Find("Tile" + i + "X" + j).transform.Find("Ball") == null)
		String_t* L_5;
		L_5 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_0), NULL);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_1), NULL);
		String_t* L_7;
		L_7 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_5, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_6, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8;
		L_8 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_7, NULL);
		NullCheck(L_8);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_9;
		L_9 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_8, NULL);
		NullCheck(L_9);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_10;
		L_10 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_9, _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_11;
		L_11 = Object_op_Equality_mD3DB0D72CE0250C84033DC2A90AEF9D59896E536(L_10, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_11)
		{
			goto IL_0089;
		}
	}
	{
		// Vars.fields[i, j] = placeholderBalls[i, j];
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_12 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_15 = __this->___placeholderBalls_10;
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		NullCheck(L_15);
		int32_t L_18;
		L_18 = (L_15)->GetAt(L_16, L_17);
		NullCheck(L_12);
		(L_12)->SetAt(L_13, L_14, L_18);
		// placeholderBalls[i, j] = 0;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_19 = __this->___placeholderBalls_10;
		int32_t L_20 = V_0;
		int32_t L_21 = V_1;
		NullCheck(L_19);
		(L_19)->SetAt(L_20, L_21, 0);
	}

IL_0089:
	{
		// GameObject ball = GameObject.Find("Tile" + i + "X" + j).transform.Find("BallPlaceholder").gameObject;
		String_t* L_22;
		L_22 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_0), NULL);
		String_t* L_23;
		L_23 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_1), NULL);
		String_t* L_24;
		L_24 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_22, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_23, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_25;
		L_25 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_24, NULL);
		NullCheck(L_25);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_26;
		L_26 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_25, NULL);
		NullCheck(L_26);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_27;
		L_27 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_26, _stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0, NULL);
		NullCheck(L_27);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_27, NULL);
		// ball.name = "Ball";
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = L_28;
		NullCheck(L_29);
		Object_set_name_mC79E6DC8FFD72479C90F0C4CC7F42A0FEAF5AE47(L_29, _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, NULL);
		// ball.transform.localScale = new Vector2(1, 1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30 = L_29;
		NullCheck(L_30);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_31;
		L_31 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_30, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_32;
		memset((&L_32), 0, sizeof(L_32));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_32), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_33;
		L_33 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_32, NULL);
		NullCheck(L_31);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_31, L_33, NULL);
		// ball.GetComponent<SpriteRenderer>().sortingOrder = 0;
		NullCheck(L_30);
		SpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B* L_34;
		L_34 = GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9(L_30, GameObject_GetComponent_TisSpriteRenderer_t1DD7FE258F072E1FA87D6577BA27225892B8047B_m8EE7EDCCEECA15A55F6D81B522B17AFB14AB25F9_RuntimeMethod_var);
		NullCheck(L_34);
		Renderer_set_sortingOrder_m4C67F002AD68CA0D55D20D6B78CDED3DB24467DA(L_34, 0, NULL);
	}

IL_00f4:
	{
		// for (int j = 0; j < placeholderBalls.GetLength(1); j++)
		int32_t L_35 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_35, 1));
	}

IL_00f8:
	{
		// for (int j = 0; j < placeholderBalls.GetLength(1); j++)
		int32_t L_36 = V_1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_37 = __this->___placeholderBalls_10;
		NullCheck((RuntimeArray*)L_37);
		int32_t L_38;
		L_38 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_37, 1, NULL);
		if ((((int32_t)L_36) < ((int32_t)L_38)))
		{
			goto IL_0017;
		}
	}
	{
		// for (int i = 0; i < placeholderBalls.GetLength(0); i++)
		int32_t L_39 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_39, 1));
	}

IL_010e:
	{
		// for (int i = 0; i < placeholderBalls.GetLength(0); i++)
		int32_t L_40 = V_0;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_41 = __this->___placeholderBalls_10;
		NullCheck((RuntimeArray*)L_41);
		int32_t L_42;
		L_42 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_41, 0, NULL);
		if ((((int32_t)L_40) < ((int32_t)L_42)))
		{
			goto IL_0010;
		}
	}
	{
		// }
		return;
	}
}
// System.Void BallsPathfinder::CheckForScoreVertically()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckForScoreVertically_m77901B14D506A171956F78185326656C51FB0785 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		V_0 = 0;
		goto IL_00bf;
	}

IL_0007:
	{
		// ResetConsecuteveBallsSearchingVariables();
		BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6(__this, NULL);
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		V_1 = 0;
		goto IL_00aa;
	}

IL_0014:
	{
		// if (Vars.fields[i, j] != 0)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		NullCheck(L_0);
		int32_t L_3;
		L_3 = (L_0)->GetAt(L_1, L_2);
		if (!L_3)
		{
			goto IL_00a0;
		}
	}
	{
		// if (Vars.fields[i, j] == currentCellValue)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_4 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_5 = V_0;
		int32_t L_6 = V_1;
		NullCheck(L_4);
		int32_t L_7;
		L_7 = (L_4)->GetAt(L_5, L_6);
		int32_t L_8 = __this->___currentCellValue_13;
		if ((!(((uint32_t)L_7) == ((uint32_t)L_8))))
		{
			goto IL_0085;
		}
	}
	{
		// numberOfConsecutiveBalls++;
		int32_t L_9 = __this->___numberOfConsecutiveBalls_12;
		__this->___numberOfConsecutiveBalls_12 = ((int32_t)il2cpp_codegen_add(L_9, 1));
		// if (numberOfConsecutiveBalls >= 5)
		int32_t L_10 = __this->___numberOfConsecutiveBalls_12;
		if ((((int32_t)L_10) < ((int32_t)5)))
		{
			goto IL_00a6;
		}
	}
	{
		// for (int y = j - numberOfConsecutiveBalls + 1; y <= j; y++)
		int32_t L_11 = V_1;
		int32_t L_12 = __this->___numberOfConsecutiveBalls_12;
		V_2 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_subtract(L_11, L_12)), 1));
		goto IL_0073;
	}

IL_005a:
	{
		// Vars.fields[i, y] = 0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_13 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_14 = V_0;
		int32_t L_15 = V_2;
		NullCheck(L_13);
		(L_13)->SetAt(L_14, L_15, 0);
		// DestoyBall(i, y);
		int32_t L_16 = V_0;
		int32_t L_17 = V_2;
		BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE(__this, L_16, L_17, NULL);
		// for (int y = j - numberOfConsecutiveBalls + 1; y <= j; y++)
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_18, 1));
	}

IL_0073:
	{
		// for (int y = j - numberOfConsecutiveBalls + 1; y <= j; y++)
		int32_t L_19 = V_2;
		int32_t L_20 = V_1;
		if ((((int32_t)L_19) <= ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		// Vars.score++;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_21 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9 = ((int32_t)il2cpp_codegen_add(L_21, 1));
		goto IL_00a6;
	}

IL_0085:
	{
		// currentCellValue = Vars.fields[i, j];
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_22 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		NullCheck(L_22);
		int32_t L_25;
		L_25 = (L_22)->GetAt(L_23, L_24);
		__this->___currentCellValue_13 = L_25;
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		goto IL_00a6;
	}

IL_00a0:
	{
		// ResetConsecuteveBallsSearchingVariables();
		BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6(__this, NULL);
	}

IL_00a6:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_26 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_26, 1));
	}

IL_00aa:
	{
		// for (int j = 0; j < Vars.fields.GetLength(1); j++)
		int32_t L_27 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_28 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_28);
		int32_t L_29;
		L_29 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_28, 1, NULL);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_0014;
		}
	}
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_30 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_30, 1));
	}

IL_00bf:
	{
		// for (int i = 0; i < Vars.fields.GetLength(0); i++)
		int32_t L_31 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_32 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_32);
		int32_t L_33;
		L_33 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_32, 0, NULL);
		if ((((int32_t)L_31) < ((int32_t)L_33)))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Void BallsPathfinder::CheckForScoreHorizontally()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckForScoreHorizontally_m079A65463A19BB59EA49B8C9530061206A2BFAC1 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// for (int i = 0; i < Vars.fields.GetLength(1); i++)
		V_0 = 0;
		goto IL_00bf;
	}

IL_0007:
	{
		// ResetConsecuteveBallsSearchingVariables();
		BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6(__this, NULL);
		// for (int j = 0; j < Vars.fields.GetLength(0); j++)
		V_1 = 0;
		goto IL_00aa;
	}

IL_0014:
	{
		// if (Vars.fields[j, i] != 0)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		NullCheck(L_0);
		int32_t L_3;
		L_3 = (L_0)->GetAt(L_1, L_2);
		if (!L_3)
		{
			goto IL_00a0;
		}
	}
	{
		// if (Vars.fields[j, i] == currentCellValue)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_4 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		NullCheck(L_4);
		int32_t L_7;
		L_7 = (L_4)->GetAt(L_5, L_6);
		int32_t L_8 = __this->___currentCellValue_13;
		if ((!(((uint32_t)L_7) == ((uint32_t)L_8))))
		{
			goto IL_0085;
		}
	}
	{
		// numberOfConsecutiveBalls++;
		int32_t L_9 = __this->___numberOfConsecutiveBalls_12;
		__this->___numberOfConsecutiveBalls_12 = ((int32_t)il2cpp_codegen_add(L_9, 1));
		// if (numberOfConsecutiveBalls >= 5)
		int32_t L_10 = __this->___numberOfConsecutiveBalls_12;
		if ((((int32_t)L_10) < ((int32_t)5)))
		{
			goto IL_00a6;
		}
	}
	{
		// for (int y = j - numberOfConsecutiveBalls + 1; y <= j; y++)
		int32_t L_11 = V_1;
		int32_t L_12 = __this->___numberOfConsecutiveBalls_12;
		V_2 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_subtract(L_11, L_12)), 1));
		goto IL_0073;
	}

IL_005a:
	{
		// Vars.fields[y, i] = 0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_13 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_14 = V_2;
		int32_t L_15 = V_0;
		NullCheck(L_13);
		(L_13)->SetAt(L_14, L_15, 0);
		// DestoyBall(y, i);
		int32_t L_16 = V_2;
		int32_t L_17 = V_0;
		BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE(__this, L_16, L_17, NULL);
		// for (int y = j - numberOfConsecutiveBalls + 1; y <= j; y++)
		int32_t L_18 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_18, 1));
	}

IL_0073:
	{
		// for (int y = j - numberOfConsecutiveBalls + 1; y <= j; y++)
		int32_t L_19 = V_2;
		int32_t L_20 = V_1;
		if ((((int32_t)L_19) <= ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		// Vars.score++;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_21 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9 = ((int32_t)il2cpp_codegen_add(L_21, 1));
		goto IL_00a6;
	}

IL_0085:
	{
		// currentCellValue = Vars.fields[j, i];
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_22 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_23 = V_1;
		int32_t L_24 = V_0;
		NullCheck(L_22);
		int32_t L_25;
		L_25 = (L_22)->GetAt(L_23, L_24);
		__this->___currentCellValue_13 = L_25;
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		goto IL_00a6;
	}

IL_00a0:
	{
		// ResetConsecuteveBallsSearchingVariables();
		BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6(__this, NULL);
	}

IL_00a6:
	{
		// for (int j = 0; j < Vars.fields.GetLength(0); j++)
		int32_t L_26 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_26, 1));
	}

IL_00aa:
	{
		// for (int j = 0; j < Vars.fields.GetLength(0); j++)
		int32_t L_27 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_28 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_28);
		int32_t L_29;
		L_29 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_28, 0, NULL);
		if ((((int32_t)L_27) < ((int32_t)L_29)))
		{
			goto IL_0014;
		}
	}
	{
		// for (int i = 0; i < Vars.fields.GetLength(1); i++)
		int32_t L_30 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_30, 1));
	}

IL_00bf:
	{
		// for (int i = 0; i < Vars.fields.GetLength(1); i++)
		int32_t L_31 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_32 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_32);
		int32_t L_33;
		L_33 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_32, 1, NULL);
		if ((((int32_t)L_31) < ((int32_t)L_33)))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Void BallsPathfinder::CheckForScoreDiagonally()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckForScoreDiagonally_m0D8C80E75B167112F06671DDE6175BF698820B80 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		// for (int line = 1; line <= (Vars.fields.GetLength(0) + Vars.fields.GetLength(1) - 1); line++)
		V_0 = 1;
		goto IL_0170;
	}

IL_0007:
	{
		// ResetConsecuteveBallsSearchingVariables();
		BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6(__this, NULL);
		// int start_col = Max(0, line - Vars.fields.GetLength(0));
		int32_t L_0 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_1 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_1);
		int32_t L_2;
		L_2 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_1, 0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		int32_t L_3;
		L_3 = Math_Max_m830F00B616D7A2130E46E974DFB27E9DA7FE30E5(0, ((int32_t)il2cpp_codegen_subtract(L_0, L_2)), NULL);
		V_1 = L_3;
		// int count = Min(line, Math.Min((Vars.fields.GetLength(1) - start_col), Vars.fields.GetLength(0)));
		int32_t L_4 = V_0;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_5 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_5);
		int32_t L_6;
		L_6 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_5, 1, NULL);
		int32_t L_7 = V_1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_8 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_8);
		int32_t L_9;
		L_9 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_8, 0, NULL);
		int32_t L_10;
		L_10 = Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8(((int32_t)il2cpp_codegen_subtract(L_6, L_7)), L_9, NULL);
		int32_t L_11;
		L_11 = Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8(L_4, L_10, NULL);
		V_2 = L_11;
		// for (int j = 0; j < count; j++)
		V_3 = 0;
		goto IL_0165;
	}

IL_004c:
	{
		// if (Vars.fields[Min(Vars.fields.GetLength(0), line) - j - 1, start_col + j] != 0)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_12 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_13 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_13);
		int32_t L_14;
		L_14 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_13, 0, NULL);
		int32_t L_15 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		int32_t L_16;
		L_16 = Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8(L_14, L_15, NULL);
		int32_t L_17 = V_3;
		int32_t L_18 = V_1;
		int32_t L_19 = V_3;
		NullCheck(L_12);
		int32_t L_20;
		L_20 = (L_12)->GetAt(((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_subtract(L_16, L_17)), 1)), ((int32_t)il2cpp_codegen_add(L_18, L_19)));
		if (!L_20)
		{
			goto IL_015b;
		}
	}
	{
		// if (Vars.fields[Min(Vars.fields.GetLength(0), line) - j - 1, start_col + j] == currentCellValue)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_21 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_22 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_22);
		int32_t L_23;
		L_23 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_22, 0, NULL);
		int32_t L_24 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		int32_t L_25;
		L_25 = Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8(L_23, L_24, NULL);
		int32_t L_26 = V_3;
		int32_t L_27 = V_1;
		int32_t L_28 = V_3;
		NullCheck(L_21);
		int32_t L_29;
		L_29 = (L_21)->GetAt(((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_subtract(L_25, L_26)), 1)), ((int32_t)il2cpp_codegen_add(L_27, L_28)));
		int32_t L_30 = __this->___currentCellValue_13;
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_012a;
		}
	}
	{
		// numberOfConsecutiveBalls++;
		int32_t L_31 = __this->___numberOfConsecutiveBalls_12;
		__this->___numberOfConsecutiveBalls_12 = ((int32_t)il2cpp_codegen_add(L_31, 1));
		// if (numberOfConsecutiveBalls >= 5)
		int32_t L_32 = __this->___numberOfConsecutiveBalls_12;
		if ((((int32_t)L_32) < ((int32_t)5)))
		{
			goto IL_0161;
		}
	}
	{
		// for (int y = 0; y < numberOfConsecutiveBalls; y++)
		V_4 = 0;
		goto IL_0112;
	}

IL_00bf:
	{
		// Vars.fields[Min(Vars.fields.GetLength(0), line) - j - 1 + y, start_col + j - y] = 0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_33 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_34 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_34);
		int32_t L_35;
		L_35 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_34, 0, NULL);
		int32_t L_36 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		int32_t L_37;
		L_37 = Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8(L_35, L_36, NULL);
		int32_t L_38 = V_3;
		int32_t L_39 = V_4;
		int32_t L_40 = V_1;
		int32_t L_41 = V_3;
		int32_t L_42 = V_4;
		NullCheck(L_33);
		(L_33)->SetAt(((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_subtract(L_37, L_38)), 1)), L_39)), ((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_add(L_40, L_41)), L_42)), 0);
		// DestoyBall((Min(Vars.fields.GetLength(0), line) - j - 1 + y), start_col + j - y);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_43 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_43);
		int32_t L_44;
		L_44 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_43, 0, NULL);
		int32_t L_45 = V_0;
		int32_t L_46;
		L_46 = Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8(L_44, L_45, NULL);
		int32_t L_47 = V_3;
		int32_t L_48 = V_4;
		int32_t L_49 = V_1;
		int32_t L_50 = V_3;
		int32_t L_51 = V_4;
		BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE(__this, ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_subtract(L_46, L_47)), 1)), L_48)), ((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_add(L_49, L_50)), L_51)), NULL);
		// for (int y = 0; y < numberOfConsecutiveBalls; y++)
		int32_t L_52 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_52, 1));
	}

IL_0112:
	{
		// for (int y = 0; y < numberOfConsecutiveBalls; y++)
		int32_t L_53 = V_4;
		int32_t L_54 = __this->___numberOfConsecutiveBalls_12;
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_00bf;
		}
	}
	{
		// Vars.score++;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_55 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9 = ((int32_t)il2cpp_codegen_add(L_55, 1));
		goto IL_0161;
	}

IL_012a:
	{
		// currentCellValue = Vars.fields[Min(Vars.fields.GetLength(0), line) - j - 1, start_col + j];
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_56 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_57 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_57);
		int32_t L_58;
		L_58 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_57, 0, NULL);
		int32_t L_59 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		int32_t L_60;
		L_60 = Math_Min_m1F346FEDDC77AC1EC0C4EF1AC6BA59F0EC7980F8(L_58, L_59, NULL);
		int32_t L_61 = V_3;
		int32_t L_62 = V_1;
		int32_t L_63 = V_3;
		NullCheck(L_56);
		int32_t L_64;
		L_64 = (L_56)->GetAt(((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_subtract(L_60, L_61)), 1)), ((int32_t)il2cpp_codegen_add(L_62, L_63)));
		__this->___currentCellValue_13 = L_64;
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		goto IL_0161;
	}

IL_015b:
	{
		// ResetConsecuteveBallsSearchingVariables();
		BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6(__this, NULL);
	}

IL_0161:
	{
		// for (int j = 0; j < count; j++)
		int32_t L_65 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_65, 1));
	}

IL_0165:
	{
		// for (int j = 0; j < count; j++)
		int32_t L_66 = V_3;
		int32_t L_67 = V_2;
		if ((((int32_t)L_66) < ((int32_t)L_67)))
		{
			goto IL_004c;
		}
	}
	{
		// for (int line = 1; line <= (Vars.fields.GetLength(0) + Vars.fields.GetLength(1) - 1); line++)
		int32_t L_68 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add(L_68, 1));
	}

IL_0170:
	{
		// for (int line = 1; line <= (Vars.fields.GetLength(0) + Vars.fields.GetLength(1) - 1); line++)
		int32_t L_69 = V_0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_70 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_70);
		int32_t L_71;
		L_71 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_70, 0, NULL);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_72 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_72);
		int32_t L_73;
		L_73 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_72, 1, NULL);
		if ((((int32_t)L_69) <= ((int32_t)((int32_t)il2cpp_codegen_subtract(((int32_t)il2cpp_codegen_add(L_71, L_73)), 1)))))
		{
			goto IL_0007;
		}
	}
	{
		// }
		return;
	}
}
// System.Boolean BallsPathfinder::CheckForScoreDiagonallyTraverse(System.Int32[,],System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BallsPathfinder_CheckForScoreDiagonallyTraverse_m38B44ED6144820ABF5C94721B87FC4B475D84C7A (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___m0, int32_t ___i1, int32_t ___j2, int32_t ___row3, int32_t ___col4, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// if (i >= row || j >= col)
		int32_t L_0 = ___i1;
		int32_t L_1 = ___row3;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_000a;
		}
	}
	{
		int32_t L_2 = ___j2;
		int32_t L_3 = ___col4;
		if ((((int32_t)L_2) < ((int32_t)L_3)))
		{
			goto IL_0084;
		}
	}

IL_000a:
	{
		// if (flag)
		bool L_4 = __this->___flag_16;
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		// int a = k1;
		int32_t L_5 = __this->___k1_14;
		V_0 = L_5;
		// k1 = k2;
		int32_t L_6 = __this->___k2_15;
		__this->___k1_14 = L_6;
		// k2 = a;
		int32_t L_7 = V_0;
		__this->___k2_15 = L_7;
		// flag = !flag;
		bool L_8 = __this->___flag_16;
		__this->___flag_16 = (bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0);
		// k1++;
		int32_t L_9 = __this->___k1_14;
		__this->___k1_14 = ((int32_t)il2cpp_codegen_add(L_9, 1));
		goto IL_0074;
	}

IL_004b:
	{
		// int a = k1;
		int32_t L_10 = __this->___k1_14;
		V_1 = L_10;
		// k1 = k2;
		int32_t L_11 = __this->___k2_15;
		__this->___k1_14 = L_11;
		// k2 = a;
		int32_t L_12 = V_1;
		__this->___k2_15 = L_12;
		// flag = !flag;
		bool L_13 = __this->___flag_16;
		__this->___flag_16 = (bool)((((int32_t)L_13) == ((int32_t)0))? 1 : 0);
	}

IL_0074:
	{
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		// currentCellValue = 0;
		__this->___currentCellValue_13 = 0;
		// return false;
		return (bool)0;
	}

IL_0084:
	{
		// if (Vars.fields[i, j] != 0)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_14 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_15 = ___i1;
		int32_t L_16 = ___j2;
		NullCheck(L_14);
		int32_t L_17;
		L_17 = (L_14)->GetAt(L_15, L_16);
		if (!L_17)
		{
			goto IL_0117;
		}
	}
	{
		// if (Vars.fields[i, j] == currentCellValue)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_18 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_19 = ___i1;
		int32_t L_20 = ___j2;
		NullCheck(L_18);
		int32_t L_21;
		L_21 = (L_18)->GetAt(L_19, L_20);
		int32_t L_22 = __this->___currentCellValue_13;
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_00fc;
		}
	}
	{
		// numberOfConsecutiveBalls++;
		int32_t L_23 = __this->___numberOfConsecutiveBalls_12;
		__this->___numberOfConsecutiveBalls_12 = ((int32_t)il2cpp_codegen_add(L_23, 1));
		// if (numberOfConsecutiveBalls >= 5)
		int32_t L_24 = __this->___numberOfConsecutiveBalls_12;
		if ((((int32_t)L_24) < ((int32_t)5)))
		{
			goto IL_0125;
		}
	}
	{
		// for (int y = 0; y < numberOfConsecutiveBalls; y++)
		V_2 = 0;
		goto IL_00e5;
	}

IL_00c4:
	{
		// Vars.fields[i - y, j - y] = 0;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_25 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_26 = ___i1;
		int32_t L_27 = V_2;
		int32_t L_28 = ___j2;
		int32_t L_29 = V_2;
		NullCheck(L_25);
		(L_25)->SetAt(((int32_t)il2cpp_codegen_subtract(L_26, L_27)), ((int32_t)il2cpp_codegen_subtract(L_28, L_29)), 0);
		// DestoyBall((i - y), (j - y));
		int32_t L_30 = ___i1;
		int32_t L_31 = V_2;
		int32_t L_32 = ___j2;
		int32_t L_33 = V_2;
		BallsPathfinder_DestoyBall_m3E74A879DFD7C7E61B67D0E3D695961727AE14CE(__this, ((int32_t)il2cpp_codegen_subtract(L_30, L_31)), ((int32_t)il2cpp_codegen_subtract(L_32, L_33)), NULL);
		// for (int y = 0; y < numberOfConsecutiveBalls; y++)
		int32_t L_34 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_34, 1));
	}

IL_00e5:
	{
		// for (int y = 0; y < numberOfConsecutiveBalls; y++)
		int32_t L_35 = V_2;
		int32_t L_36 = __this->___numberOfConsecutiveBalls_12;
		if ((((int32_t)L_35) < ((int32_t)L_36)))
		{
			goto IL_00c4;
		}
	}
	{
		// Vars.score++;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_37 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9 = ((int32_t)il2cpp_codegen_add(L_37, 1));
		goto IL_0125;
	}

IL_00fc:
	{
		// currentCellValue = Vars.fields[i, j];
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_38 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_39 = ___i1;
		int32_t L_40 = ___j2;
		NullCheck(L_38);
		int32_t L_41;
		L_41 = (L_38)->GetAt(L_39, L_40);
		__this->___currentCellValue_13 = L_41;
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		goto IL_0125;
	}

IL_0117:
	{
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		// currentCellValue = 0;
		__this->___currentCellValue_13 = 0;
	}

IL_0125:
	{
		// if (CheckForScoreDiagonallyTraverse(m, i + 1, j + 1, row, col))
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_42 = ___m0;
		int32_t L_43 = ___i1;
		int32_t L_44 = ___j2;
		int32_t L_45 = ___row3;
		int32_t L_46 = ___col4;
		bool L_47;
		L_47 = BallsPathfinder_CheckForScoreDiagonallyTraverse_m38B44ED6144820ABF5C94721B87FC4B475D84C7A(__this, L_42, ((int32_t)il2cpp_codegen_add(L_43, 1)), ((int32_t)il2cpp_codegen_add(L_44, 1)), L_45, L_46, NULL);
		if (!L_47)
		{
			goto IL_013a;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_013a:
	{
		// if (CheckForScoreDiagonallyTraverse(m, k1, k2, row, col))
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_48 = ___m0;
		int32_t L_49 = __this->___k1_14;
		int32_t L_50 = __this->___k2_15;
		int32_t L_51 = ___row3;
		int32_t L_52 = ___col4;
		bool L_53;
		L_53 = BallsPathfinder_CheckForScoreDiagonallyTraverse_m38B44ED6144820ABF5C94721B87FC4B475D84C7A(__this, L_48, L_49, L_50, L_51, L_52, NULL);
		// return true;
		return (bool)1;
	}
}
// System.Void BallsPathfinder::CheckScore()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_CheckScore_m6345AE27461D331492D27D28E16945CCF09411F5 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CheckForScoreVertically();
		BallsPathfinder_CheckForScoreVertically_m77901B14D506A171956F78185326656C51FB0785(__this, NULL);
		// CheckForScoreHorizontally();
		BallsPathfinder_CheckForScoreHorizontally_m079A65463A19BB59EA49B8C9530061206A2BFAC1(__this, NULL);
		// CheckForScoreDiagonally();
		BallsPathfinder_CheckForScoreDiagonally_m0D8C80E75B167112F06671DDE6175BF698820B80(__this, NULL);
		// ResetConsecuteveBallsSearchingVariables();
		BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6(__this, NULL);
		// CheckForScoreDiagonallyTraverse(Vars.fields, 0, 0, Vars.fields.GetLength(0), Vars.fields.GetLength(1));
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_1 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_1);
		int32_t L_2;
		L_2 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_1, 0, NULL);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_3 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		NullCheck((RuntimeArray*)L_3);
		int32_t L_4;
		L_4 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_3, 1, NULL);
		bool L_5;
		L_5 = BallsPathfinder_CheckForScoreDiagonallyTraverse_m38B44ED6144820ABF5C94721B87FC4B475D84C7A(__this, L_0, 0, 0, L_2, L_4, NULL);
		// GetComponent<Menus>().UpdateScore();
		Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* L_6;
		L_6 = Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5(__this, Component_GetComponent_TisMenus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540_mF1A05C3093863B08A1BE8BEAF393B820DB873AC5_RuntimeMethod_var);
		NullCheck(L_6);
		Menus_UpdateScore_m6D7C68AE360246A76F901FE59837276BF601E8DC(L_6, NULL);
		// }
		return;
	}
}
// System.Void BallsPathfinder::ResetConsecuteveBallsSearchingVariables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_ResetConsecuteveBallsSearchingVariables_m799200041523E604496C2F82808F7E0B6F78FCC6 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	{
		// numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		// currentCellValue = 0;
		__this->___currentCellValue_13 = 0;
		// k1 = 0;
		__this->___k1_14 = 0;
		// k2 = 0;
		__this->___k2_15 = 0;
		// flag = true;
		__this->___flag_16 = (bool)1;
		// }
		return;
	}
}
// System.Void BallsPathfinder::InitializeBallMovement(UnityEngine.GameObject,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_InitializeBallMovement_m9E10C61F99868F73E615449DF46689DD33CD340B (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___ball0, int32_t ___xTarget1, int32_t ___yTarget2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA5026BA636AED7E7171601C8E89BFD371EADA710);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFCAC1F6985AA9B53AA3AA1B94148E11F91CCAE4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* V_1 = NULL;
	{
		// int ballColor = Vars.fields[Vars.ballStartPosX, Vars.ballStartPosY];
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_1 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5;
		int32_t L_2 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6;
		NullCheck(L_0);
		int32_t L_3;
		L_3 = (L_0)->GetAt(L_1, L_2);
		V_0 = L_3;
		// Vars.fields[Vars.ballStartPosX, Vars.ballStartPosY] = 0;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_4 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_5 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5;
		int32_t L_6 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6;
		NullCheck(L_4);
		(L_4)->SetAt(L_5, L_6, 0);
		// int[,] path = FindPath(Vars.ballStartPosX, Vars.ballStartPosY, xTarget, yTarget);
		int32_t L_7 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5;
		int32_t L_8 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6;
		int32_t L_9 = ___xTarget1;
		int32_t L_10 = ___yTarget2;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_11;
		L_11 = BallsPathfinder_FindPath_m7FA847FADB1462CBBB01437FE18C89FFD28B5597(__this, L_7, L_8, L_9, L_10, NULL);
		V_1 = L_11;
		// if (path != null)
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_12 = V_1;
		if (!L_12)
		{
			goto IL_0099;
		}
	}
	{
		// Vars.ball.GetComponent<SelectedBallAnimation>().enabled = false;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_13);
		SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* L_14;
		L_14 = GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070(L_13, GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070_RuntimeMethod_var);
		NullCheck(L_14);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_14, (bool)0, NULL);
		// Vars.ball.transform.localScale = new Vector2(1, 1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_15 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_15);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_16;
		L_16 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_15, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_17;
		memset((&L_17), 0, sizeof(L_17));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_17), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_18;
		L_18 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_17, NULL);
		NullCheck(L_16);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_16, L_18, NULL);
		// StartCoroutine(BallMovement(path, xTarget, yTarget, ballColor));//Move the ball to the choosen location
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_19 = V_1;
		int32_t L_20 = ___xTarget1;
		int32_t L_21 = ___yTarget2;
		int32_t L_22 = V_0;
		RuntimeObject* L_23;
		L_23 = BallsPathfinder_BallMovement_m48DD0BF5B50C07413E4D143C31463126971EEB49(__this, L_19, L_20, L_21, L_22, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_24;
		L_24 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_23, NULL);
		// GameObject.Find("BallMoveSound").GetComponent<AudioSource>().Play();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_25;
		L_25 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralA5026BA636AED7E7171601C8E89BFD371EADA710, NULL);
		NullCheck(L_25);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_26;
		L_26 = GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A(L_25, GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		NullCheck(L_26);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_26, NULL);
		return;
	}

IL_0099:
	{
		// Vars.fields[Vars.ballStartPosX, Vars.ballStartPosY] = ballColor;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_27 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_28 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5;
		int32_t L_29 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6;
		int32_t L_30 = V_0;
		NullCheck(L_27);
		(L_27)->SetAt(L_28, L_29, L_30);
		// Vars.ball.transform.parent = GameObject.Find("Tile" + Vars.ballStartPosX + "X" + Vars.ballStartPosY).transform;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_31 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_31);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_32;
		L_32 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_31, NULL);
		String_t* L_33;
		L_33 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5), NULL);
		String_t* L_34;
		L_34 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6), NULL);
		String_t* L_35;
		L_35 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_33, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_34, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_36;
		L_36 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_35, NULL);
		NullCheck(L_36);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_37;
		L_37 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_36, NULL);
		NullCheck(L_32);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_32, L_37, NULL);
		// GameObject.Find("NoAvailableMoveSound").GetComponent<AudioSource>().Play();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_38;
		L_38 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralBFCAC1F6985AA9B53AA3AA1B94148E11F91CCAE4, NULL);
		NullCheck(L_38);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_39;
		L_39 = GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A(L_38, GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		NullCheck(L_39);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_39, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator BallsPathfinder::BallMovement(System.Int32[,],System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* BallsPathfinder_BallMovement_m48DD0BF5B50C07413E4D143C31463126971EEB49 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___path0, int32_t ___xTarget1, int32_t ___yTarget2, int32_t ___ballColor3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* L_0 = (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63*)il2cpp_codegen_object_new(U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63_il2cpp_TypeInfo_var);
		NullCheck(L_0);
		U3CBallMovementU3Ed__29__ctor_mE3D83FBDE142D6E79AC3714B748B82A3379152B1(L_0, 0, NULL);
		U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* L_1 = L_0;
		NullCheck(L_1);
		L_1->___U3CU3E4__this_6 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_6), (void*)__this);
		U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* L_2 = L_1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_3 = ___path0;
		NullCheck(L_2);
		L_2->___path_2 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&L_2->___path_2), (void*)L_3);
		U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* L_4 = L_2;
		int32_t L_5 = ___xTarget1;
		NullCheck(L_4);
		L_4->___xTarget_3 = L_5;
		U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* L_6 = L_4;
		int32_t L_7 = ___yTarget2;
		NullCheck(L_6);
		L_6->___yTarget_4 = L_7;
		U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* L_8 = L_6;
		int32_t L_9 = ___ballColor3;
		NullCheck(L_8);
		L_8->___ballColor_5 = L_9;
		return L_8;
	}
}
// System.Int32 BallsPathfinder::get_Rows()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BallsPathfinder_get_Rows_mE5A605AC48FA7C0E3602B21226B15FC04F63B13B (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	{
		// get { return numberOfRows; }
		int32_t L_0 = __this->___numberOfRows_4;
		return L_0;
	}
}
// System.Int32 BallsPathfinder::get_Cols()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BallsPathfinder_get_Cols_m2D237BCDE09D2ED862D8855D71B86BDBCDE01C14 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	{
		// get { return numberOfColumns; }
		int32_t L_0 = __this->___numberOfColumns_5;
		return L_0;
	}
}
// System.Int32 BallsPathfinder::GetNodeContents(System.Int32[,],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___iMaze0, int32_t ___iNodeNo1, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// int iCols = iMaze.GetLength(1);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = ___iMaze0;
		NullCheck((RuntimeArray*)L_0);
		int32_t L_1;
		L_1 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_0, 1, NULL);
		V_0 = L_1;
		// return iMaze[iNodeNo / iCols, iNodeNo - iNodeNo / iCols * iCols];
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_2 = ___iMaze0;
		int32_t L_3 = ___iNodeNo1;
		int32_t L_4 = V_0;
		int32_t L_5 = ___iNodeNo1;
		int32_t L_6 = ___iNodeNo1;
		int32_t L_7 = V_0;
		int32_t L_8 = V_0;
		NullCheck(L_2);
		int32_t L_9;
		L_9 = (L_2)->GetAt(((int32_t)(L_3/L_4)), ((int32_t)il2cpp_codegen_subtract(L_5, ((int32_t)il2cpp_codegen_multiply(((int32_t)(L_6/L_7)), L_8)))));
		return L_9;
	}
}
// System.Void BallsPathfinder::ChangeNodeContents(System.Int32[,],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* ___iMaze0, int32_t ___iNodeNo1, int32_t ___iNewValue2, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		// int iCols = iMaze.GetLength(1);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_0 = ___iMaze0;
		NullCheck((RuntimeArray*)L_0);
		int32_t L_1;
		L_1 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_0, 1, NULL);
		V_0 = L_1;
		// iMaze[iNodeNo / iCols, iNodeNo - iNodeNo / iCols * iCols] = iNewValue;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_2 = ___iMaze0;
		int32_t L_3 = ___iNodeNo1;
		int32_t L_4 = V_0;
		int32_t L_5 = ___iNodeNo1;
		int32_t L_6 = ___iNodeNo1;
		int32_t L_7 = V_0;
		int32_t L_8 = V_0;
		int32_t L_9 = ___iNewValue2;
		NullCheck(L_2);
		(L_2)->SetAt(((int32_t)(L_3/L_4)), ((int32_t)il2cpp_codegen_subtract(L_5, ((int32_t)il2cpp_codegen_multiply(((int32_t)(L_6/L_7)), L_8)))), L_9);
		// }
		return;
	}
}
// System.Int32[,] BallsPathfinder::FindPath(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* BallsPathfinder_FindPath_m7FA847FADB1462CBBB01437FE18C89FFD28B5597 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___iFromY0, int32_t ___iFromX1, int32_t ___iToY2, int32_t ___iToX3, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// int iBeginningNode = iFromY * this.Cols + iFromX;
		int32_t L_0 = ___iFromY0;
		int32_t L_1;
		L_1 = BallsPathfinder_get_Cols_m2D237BCDE09D2ED862D8855D71B86BDBCDE01C14_inline(__this, NULL);
		int32_t L_2 = ___iFromX1;
		V_0 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_0, L_1)), L_2));
		// int iEndingNode = iToY * this.Cols + iToX;
		int32_t L_3 = ___iToY2;
		int32_t L_4;
		L_4 = BallsPathfinder_get_Cols_m2D237BCDE09D2ED862D8855D71B86BDBCDE01C14_inline(__this, NULL);
		int32_t L_5 = ___iToX3;
		V_1 = ((int32_t)il2cpp_codegen_add(((int32_t)il2cpp_codegen_multiply(L_3, L_4)), L_5));
		// return (Search(iBeginningNode, iEndingNode));
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_8;
		L_8 = BallsPathfinder_Search_m29FA4638DD4989B518626DC247A9CCCE00CD7F72(__this, L_6, L_7, NULL);
		return L_8;
	}
}
// System.Int32[,] BallsPathfinder::Search(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* BallsPathfinder_Search_m29FA4638DD4989B518626DC247A9CCCE00CD7F72 (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, int32_t ___iStart0, int32_t ___iStop1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_3 = NULL;
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* V_13 = NULL;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	{
		// int iRows = numberOfRows;
		int32_t L_0 = __this->___numberOfRows_4;
		V_0 = L_0;
		// int iCols = numberOfColumns;
		int32_t L_1 = __this->___numberOfColumns_5;
		V_1 = L_1;
		// int iMax = iRows * iCols;
		int32_t L_2 = V_0;
		int32_t L_3 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_multiply(L_2, L_3));
		// int[] Queue = new int[iMax];
		int32_t L_4 = V_2;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_5 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)L_4);
		V_3 = L_5;
		// int[] Origin = new int[iMax];
		int32_t L_6 = V_2;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_7 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)SZArrayNew(Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C_il2cpp_TypeInfo_var, (uint32_t)L_6);
		V_4 = L_7;
		// int iFront = 0, iRear = 0;
		V_5 = 0;
		// int iFront = 0, iRear = 0;
		V_6 = 0;
		// if (GetNodeContents(Vars.fields, iStart) != empty || GetNodeContents(Vars.fields, iStop) != empty) return null;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_8 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_9 = ___iStart0;
		int32_t L_10;
		L_10 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_8, L_9, NULL);
		if (L_10)
		{
			goto IL_0043;
		}
	}
	{
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_11 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_12 = ___iStop1;
		int32_t L_13;
		L_13 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_11, L_12, NULL);
		if (!L_13)
		{
			goto IL_0045;
		}
	}

IL_0043:
	{
		// if (GetNodeContents(Vars.fields, iStart) != empty || GetNodeContents(Vars.fields, iStop) != empty) return null;
		return (Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)NULL;
	}

IL_0045:
	{
		// int[,] iMazeStatus = new int[iRows, iCols];
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		il2cpp_array_size_t L_17[] = { (il2cpp_array_size_t)L_14, (il2cpp_array_size_t)L_15 };
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_16 = (Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)GenArrayNew(Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var, L_17);
		V_7 = L_16;
		// for (int i = 0; i < iRows; i++)
		V_14 = 0;
		goto IL_0075;
	}

IL_0053:
	{
		// for (int j = 0; j < iCols; j++)
		V_15 = 0;
		goto IL_006a;
	}

IL_0058:
	{
		// iMazeStatus[i, j] = (int)Status.Ready;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_18 = V_7;
		int32_t L_19 = V_14;
		int32_t L_20 = V_15;
		NullCheck(L_18);
		(L_18)->SetAt(L_19, L_20, 0);
		// for (int j = 0; j < iCols; j++)
		int32_t L_21 = V_15;
		V_15 = ((int32_t)il2cpp_codegen_add(L_21, 1));
	}

IL_006a:
	{
		// for (int j = 0; j < iCols; j++)
		int32_t L_22 = V_15;
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_0058;
		}
	}
	{
		// for (int i = 0; i < iRows; i++)
		int32_t L_24 = V_14;
		V_14 = ((int32_t)il2cpp_codegen_add(L_24, 1));
	}

IL_0075:
	{
		// for (int i = 0; i < iRows; i++)
		int32_t L_25 = V_14;
		int32_t L_26 = V_0;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0053;
		}
	}
	{
		// Queue[iRear] = iStart;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_27 = V_3;
		int32_t L_28 = V_6;
		int32_t L_29 = ___iStart0;
		NullCheck(L_27);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (int32_t)L_29);
		// Origin[iRear] = -1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_30 = V_4;
		int32_t L_31 = V_6;
		NullCheck(L_30);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_31), (int32_t)(-1));
		// iRear++;
		int32_t L_32 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_32, 1));
		goto IL_01d5;
	}

IL_0090:
	{
		// if (Queue[iFront] == iStop) break;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_33 = V_3;
		int32_t L_34 = V_5;
		NullCheck(L_33);
		int32_t L_35 = L_34;
		int32_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		int32_t L_37 = ___iStop1;
		if ((((int32_t)L_36) == ((int32_t)L_37)))
		{
			goto IL_01de;
		}
	}
	{
		// iCurrent = Queue[iFront];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_38 = V_3;
		int32_t L_39 = V_5;
		NullCheck(L_38);
		int32_t L_40 = L_39;
		int32_t L_41 = (L_38)->GetAt(static_cast<il2cpp_array_size_t>(L_40));
		V_8 = L_41;
		// iLeft = iCurrent - 1;
		int32_t L_42 = V_8;
		V_9 = ((int32_t)il2cpp_codegen_subtract(L_42, 1));
		// if (iLeft >= 0 && iLeft / iCols == iCurrent / iCols)
		int32_t L_43 = V_9;
		if ((((int32_t)L_43) < ((int32_t)0)))
		{
			goto IL_00ee;
		}
	}
	{
		int32_t L_44 = V_9;
		int32_t L_45 = V_1;
		int32_t L_46 = V_8;
		int32_t L_47 = V_1;
		if ((!(((uint32_t)((int32_t)(L_44/L_45))) == ((uint32_t)((int32_t)(L_46/L_47))))))
		{
			goto IL_00ee;
		}
	}
	{
		// if (GetNodeContents(Vars.fields, iLeft) == empty)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_48 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_49 = V_9;
		int32_t L_50;
		L_50 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_48, L_49, NULL);
		if (L_50)
		{
			goto IL_00ee;
		}
	}
	{
		// if (GetNodeContents(iMazeStatus, iLeft) == (int)Status.Ready)
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_51 = V_7;
		int32_t L_52 = V_9;
		int32_t L_53;
		L_53 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_51, L_52, NULL);
		if (L_53)
		{
			goto IL_00ee;
		}
	}
	{
		// Queue[iRear] = iLeft;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_54 = V_3;
		int32_t L_55 = V_6;
		int32_t L_56 = V_9;
		NullCheck(L_54);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(L_55), (int32_t)L_56);
		// Origin[iRear] = iCurrent;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_57 = V_4;
		int32_t L_58 = V_6;
		int32_t L_59 = V_8;
		NullCheck(L_57);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(L_58), (int32_t)L_59);
		// ChangeNodeContents(iMazeStatus, iLeft, (int)Status.Waiting);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_60 = V_7;
		int32_t L_61 = V_9;
		BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783(__this, L_60, L_61, 1, NULL);
		// iRear++;
		int32_t L_62 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_62, 1));
	}

IL_00ee:
	{
		// iRight = iCurrent + 1;
		int32_t L_63 = V_8;
		V_10 = ((int32_t)il2cpp_codegen_add(L_63, 1));
		// if (iRight < iMax && iRight / iCols == iCurrent / iCols)
		int32_t L_64 = V_10;
		int32_t L_65 = V_2;
		if ((((int32_t)L_64) >= ((int32_t)L_65)))
		{
			goto IL_013c;
		}
	}
	{
		int32_t L_66 = V_10;
		int32_t L_67 = V_1;
		int32_t L_68 = V_8;
		int32_t L_69 = V_1;
		if ((!(((uint32_t)((int32_t)(L_66/L_67))) == ((uint32_t)((int32_t)(L_68/L_69))))))
		{
			goto IL_013c;
		}
	}
	{
		// if (GetNodeContents(Vars.fields, iRight) == empty)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_70 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_71 = V_10;
		int32_t L_72;
		L_72 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_70, L_71, NULL);
		if (L_72)
		{
			goto IL_013c;
		}
	}
	{
		// if (GetNodeContents(iMazeStatus, iRight) == (int)Status.Ready)
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_73 = V_7;
		int32_t L_74 = V_10;
		int32_t L_75;
		L_75 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_73, L_74, NULL);
		if (L_75)
		{
			goto IL_013c;
		}
	}
	{
		// Queue[iRear] = iRight;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_76 = V_3;
		int32_t L_77 = V_6;
		int32_t L_78 = V_10;
		NullCheck(L_76);
		(L_76)->SetAt(static_cast<il2cpp_array_size_t>(L_77), (int32_t)L_78);
		// Origin[iRear] = iCurrent;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_79 = V_4;
		int32_t L_80 = V_6;
		int32_t L_81 = V_8;
		NullCheck(L_79);
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(L_80), (int32_t)L_81);
		// ChangeNodeContents(iMazeStatus, iRight, (int)Status.Waiting);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_82 = V_7;
		int32_t L_83 = V_10;
		BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783(__this, L_82, L_83, 1, NULL);
		// iRear++;
		int32_t L_84 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_84, 1));
	}

IL_013c:
	{
		// iTop = iCurrent - iCols;
		int32_t L_85 = V_8;
		int32_t L_86 = V_1;
		V_11 = ((int32_t)il2cpp_codegen_subtract(L_85, L_86));
		// if (iTop >= 0)
		int32_t L_87 = V_11;
		if ((((int32_t)L_87) < ((int32_t)0)))
		{
			goto IL_0180;
		}
	}
	{
		// if (GetNodeContents(Vars.fields, iTop) == empty)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_88 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_89 = V_11;
		int32_t L_90;
		L_90 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_88, L_89, NULL);
		if (L_90)
		{
			goto IL_0180;
		}
	}
	{
		// if (GetNodeContents(iMazeStatus, iTop) == (int)Status.Ready)
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_91 = V_7;
		int32_t L_92 = V_11;
		int32_t L_93;
		L_93 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_91, L_92, NULL);
		if (L_93)
		{
			goto IL_0180;
		}
	}
	{
		// Queue[iRear] = iTop;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_94 = V_3;
		int32_t L_95 = V_6;
		int32_t L_96 = V_11;
		NullCheck(L_94);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(L_95), (int32_t)L_96);
		// Origin[iRear] = iCurrent;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_97 = V_4;
		int32_t L_98 = V_6;
		int32_t L_99 = V_8;
		NullCheck(L_97);
		(L_97)->SetAt(static_cast<il2cpp_array_size_t>(L_98), (int32_t)L_99);
		// ChangeNodeContents(iMazeStatus, iTop, (int)Status.Waiting);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_100 = V_7;
		int32_t L_101 = V_11;
		BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783(__this, L_100, L_101, 1, NULL);
		// iRear++;
		int32_t L_102 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_102, 1));
	}

IL_0180:
	{
		// iDown = iCurrent + iCols;
		int32_t L_103 = V_8;
		int32_t L_104 = V_1;
		V_12 = ((int32_t)il2cpp_codegen_add(L_103, L_104));
		// if (iDown < iMax)
		int32_t L_105 = V_12;
		int32_t L_106 = V_2;
		if ((((int32_t)L_105) >= ((int32_t)L_106)))
		{
			goto IL_01c4;
		}
	}
	{
		// if (GetNodeContents(Vars.fields, iDown) == empty)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_107 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_108 = V_12;
		int32_t L_109;
		L_109 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_107, L_108, NULL);
		if (L_109)
		{
			goto IL_01c4;
		}
	}
	{
		// if (GetNodeContents(iMazeStatus, iDown) == (int)Status.Ready)
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_110 = V_7;
		int32_t L_111 = V_12;
		int32_t L_112;
		L_112 = BallsPathfinder_GetNodeContents_m52EA6AD2B234DB4ACD4322580D3D5F0850A14260(__this, L_110, L_111, NULL);
		if (L_112)
		{
			goto IL_01c4;
		}
	}
	{
		// Queue[iRear] = iDown;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_113 = V_3;
		int32_t L_114 = V_6;
		int32_t L_115 = V_12;
		NullCheck(L_113);
		(L_113)->SetAt(static_cast<il2cpp_array_size_t>(L_114), (int32_t)L_115);
		// Origin[iRear] = iCurrent;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_116 = V_4;
		int32_t L_117 = V_6;
		int32_t L_118 = V_8;
		NullCheck(L_116);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(L_117), (int32_t)L_118);
		// ChangeNodeContents(iMazeStatus, iDown, (int)Status.Waiting);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_119 = V_7;
		int32_t L_120 = V_12;
		BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783(__this, L_119, L_120, 1, NULL);
		// iRear++;
		int32_t L_121 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_121, 1));
	}

IL_01c4:
	{
		// ChangeNodeContents(iMazeStatus, iCurrent, (int)Status.Processed);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_122 = V_7;
		int32_t L_123 = V_8;
		BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783(__this, L_122, L_123, 2, NULL);
		// iFront++;
		int32_t L_124 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_add(L_124, 1));
	}

IL_01d5:
	{
		// while (iFront != iRear)
		int32_t L_125 = V_5;
		int32_t L_126 = V_6;
		if ((!(((uint32_t)L_125) == ((uint32_t)L_126))))
		{
			goto IL_0090;
		}
	}

IL_01de:
	{
		// int[,] iMazeSolved = new int[iRows, iCols];
		int32_t L_127 = V_0;
		int32_t L_128 = V_1;
		il2cpp_array_size_t L_130[] = { (il2cpp_array_size_t)L_127, (il2cpp_array_size_t)L_128 };
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_129 = (Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)GenArrayNew(Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F_il2cpp_TypeInfo_var, L_130);
		V_13 = L_129;
		// for (int i = 0; i < iRows; i++)
		V_16 = 0;
		goto IL_021b;
	}

IL_01ec:
	{
		// for (int j = 0; j < iCols; j++)
		V_17 = 0;
		goto IL_0210;
	}

IL_01f1:
	{
		// iMazeSolved[i, j] = Vars.fields[i, j];
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_131 = V_13;
		int32_t L_132 = V_16;
		int32_t L_133 = V_17;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_134 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_135 = V_16;
		int32_t L_136 = V_17;
		NullCheck(L_134);
		int32_t L_137;
		L_137 = (L_134)->GetAt(L_135, L_136);
		NullCheck(L_131);
		(L_131)->SetAt(L_132, L_133, L_137);
		// for (int j = 0; j < iCols; j++)
		int32_t L_138 = V_17;
		V_17 = ((int32_t)il2cpp_codegen_add(L_138, 1));
	}

IL_0210:
	{
		// for (int j = 0; j < iCols; j++)
		int32_t L_139 = V_17;
		int32_t L_140 = V_1;
		if ((((int32_t)L_139) < ((int32_t)L_140)))
		{
			goto IL_01f1;
		}
	}
	{
		// for (int i = 0; i < iRows; i++)
		int32_t L_141 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add(L_141, 1));
	}

IL_021b:
	{
		// for (int i = 0; i < iRows; i++)
		int32_t L_142 = V_16;
		int32_t L_143 = V_0;
		if ((((int32_t)L_142) < ((int32_t)L_143)))
		{
			goto IL_01ec;
		}
	}
	{
		// iCurrent = iStop;
		int32_t L_144 = ___iStop1;
		V_8 = L_144;
		// ChangeNodeContents(iMazeSolved, iCurrent, iPath);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_145 = V_13;
		int32_t L_146 = V_8;
		int32_t L_147 = __this->___iPath_6;
		BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783(__this, L_145, L_146, L_147, NULL);
		// for (int i = iFront; i >= 0; i--)
		int32_t L_148 = V_5;
		V_18 = L_148;
		goto IL_0266;
	}

IL_0239:
	{
		// if (Queue[i] == iCurrent)
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_149 = V_3;
		int32_t L_150 = V_18;
		NullCheck(L_149);
		int32_t L_151 = L_150;
		int32_t L_152 = (L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_151));
		int32_t L_153 = V_8;
		if ((!(((uint32_t)L_152) == ((uint32_t)L_153))))
		{
			goto IL_0260;
		}
	}
	{
		// iCurrent = Origin[i];
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_154 = V_4;
		int32_t L_155 = V_18;
		NullCheck(L_154);
		int32_t L_156 = L_155;
		int32_t L_157 = (L_154)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		V_8 = L_157;
		// if (iCurrent == -1)
		int32_t L_158 = V_8;
		if ((!(((uint32_t)L_158) == ((uint32_t)(-1)))))
		{
			goto IL_0250;
		}
	}
	{
		// return (iMazeSolved);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_159 = V_13;
		return L_159;
	}

IL_0250:
	{
		// ChangeNodeContents(iMazeSolved, iCurrent, iPath);
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_160 = V_13;
		int32_t L_161 = V_8;
		int32_t L_162 = __this->___iPath_6;
		BallsPathfinder_ChangeNodeContents_m2415F192BA9ED01B50CB8C8AED3F21437B430783(__this, L_160, L_161, L_162, NULL);
	}

IL_0260:
	{
		// for (int i = iFront; i >= 0; i--)
		int32_t L_163 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_subtract(L_163, 1));
	}

IL_0266:
	{
		// for (int i = iFront; i >= 0; i--)
		int32_t L_164 = V_18;
		if ((((int32_t)L_164) >= ((int32_t)0)))
		{
			goto IL_0239;
		}
	}
	{
		// return null;
		return (Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F*)NULL;
	}
}
// System.Void BallsPathfinder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BallsPathfinder__ctor_m916FD55F58B61026AEFA0DF2573F23A3DB73C54E (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	{
		// private int iPath = 100;
		__this->___iPath_6 = ((int32_t)100);
		// private int numberOfBalls = 6;
		__this->___numberOfBalls_8 = 6;
		// private bool isFirstWave = true;
		__this->___isFirstWave_9 = (bool)1;
		// private int numberOfConsecutiveBalls = 1;
		__this->___numberOfConsecutiveBalls_12 = 1;
		// private bool flag = true;
		__this->___flag_16 = (bool)1;
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BallsPathfinder/<BallMovement>d__29::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBallMovementU3Ed__29__ctor_mE3D83FBDE142D6E79AC3714B748B82A3379152B1 (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void BallsPathfinder/<BallMovement>d__29::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBallMovementU3Ed__29_System_IDisposable_Dispose_m91C328B8B1C9304150F270CAD73CE211EC479834 (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean BallsPathfinder/<BallMovement>d__29::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CBallMovementU3Ed__29_MoveNext_mC5B4B894C674149A5F1FDBF01B9A842712143402 (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6271B5A613D8E9F509493567C383DD2671517B43);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* V_1 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_1 = __this->___U3CU3E4__this_6;
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_04af;
		}
	}
	{
		return (bool)0;
	}

IL_001a:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// Vars.isBallMoving = true;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___isBallMoving_8 = (bool)1;
		goto IL_04b6;
	}

IL_002c:
	{
		// if ((int)Vars.ball.transform.position.y > 0 && path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y - 1] == 100)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_4);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5;
		L_5 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_4, NULL);
		NullCheck(L_5);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_5, NULL);
		float L_7 = L_6.___y_3;
		if ((((int32_t)il2cpp_codegen_cast_double_to_int<int32_t>(L_7)) <= ((int32_t)0)))
		{
			goto IL_013f;
		}
	}
	{
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_8 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_9 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_9);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_10;
		L_10 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_9, NULL);
		NullCheck(L_10);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11;
		L_11 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_10, NULL);
		float L_12 = L_11.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_13);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_14;
		L_14 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_13, NULL);
		NullCheck(L_14);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15;
		L_15 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_14, NULL);
		float L_16 = L_15.___y_3;
		NullCheck(L_8);
		int32_t L_17;
		L_17 = (L_8)->GetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_12), ((int32_t)il2cpp_codegen_subtract(il2cpp_codegen_cast_double_to_int<int32_t>(L_16), 1)));
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_013f;
		}
	}
	{
		// path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_18 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_19 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_19);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_20;
		L_20 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_19, NULL);
		NullCheck(L_20);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_21;
		L_21 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_20, NULL);
		float L_22 = L_21.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_23);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_24;
		L_24 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_23, NULL);
		NullCheck(L_24);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_25;
		L_25 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_24, NULL);
		float L_26 = L_25.___y_3;
		NullCheck(L_18);
		(L_18)->SetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_22), il2cpp_codegen_cast_double_to_int<int32_t>(L_26), 1);
		// path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y - 1] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_27 = __this->___path_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_28);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_29;
		L_29 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_28, NULL);
		NullCheck(L_29);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_30;
		L_30 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_29, NULL);
		float L_31 = L_30.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_32 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_32);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_33;
		L_33 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_32, NULL);
		NullCheck(L_33);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_34;
		L_34 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_33, NULL);
		float L_35 = L_34.___y_3;
		NullCheck(L_27);
		(L_27)->SetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_31), ((int32_t)il2cpp_codegen_subtract(il2cpp_codegen_cast_double_to_int<int32_t>(L_35), 1)), 1);
		// Vars.ball.transform.position = new Vector2(Vars.ball.transform.position.x, Vars.ball.transform.position.y - 1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_36 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_36);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_37;
		L_37 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_36, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_38 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_38);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_39;
		L_39 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_38, NULL);
		NullCheck(L_39);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_40;
		L_40 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_39, NULL);
		float L_41 = L_40.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_42 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_42);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_43;
		L_43 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_42, NULL);
		NullCheck(L_43);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_44;
		L_44 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_43, NULL);
		float L_45 = L_44.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_46;
		memset((&L_46), 0, sizeof(L_46));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_46), L_41, ((float)il2cpp_codegen_subtract(L_45, (1.0f))), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_47;
		L_47 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_46, NULL);
		NullCheck(L_37);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_37, L_47, NULL);
		goto IL_0496;
	}

IL_013f:
	{
		// else if ((int)Vars.ball.transform.position.y + 1 < path.GetLength(1) && path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y + 1] == 100)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_48 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_48);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_49;
		L_49 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_48, NULL);
		NullCheck(L_49);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_50;
		L_50 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_49, NULL);
		float L_51 = L_50.___y_3;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_52 = __this->___path_2;
		NullCheck((RuntimeArray*)L_52);
		int32_t L_53;
		L_53 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_52, 1, NULL);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(il2cpp_codegen_cast_double_to_int<int32_t>(L_51), 1))) >= ((int32_t)L_53)))
		{
			goto IL_025f;
		}
	}
	{
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_54 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_55 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_55);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_56;
		L_56 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_55, NULL);
		NullCheck(L_56);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_57;
		L_57 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_56, NULL);
		float L_58 = L_57.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_59 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_59);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_60;
		L_60 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_59, NULL);
		NullCheck(L_60);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_61;
		L_61 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_60, NULL);
		float L_62 = L_61.___y_3;
		NullCheck(L_54);
		int32_t L_63;
		L_63 = (L_54)->GetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_58), ((int32_t)il2cpp_codegen_add(il2cpp_codegen_cast_double_to_int<int32_t>(L_62), 1)));
		if ((!(((uint32_t)L_63) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_025f;
		}
	}
	{
		// path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_64 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_65 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_65);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_66;
		L_66 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_65, NULL);
		NullCheck(L_66);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_67;
		L_67 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_66, NULL);
		float L_68 = L_67.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_69 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_69);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_70;
		L_70 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_69, NULL);
		NullCheck(L_70);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_71;
		L_71 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_70, NULL);
		float L_72 = L_71.___y_3;
		NullCheck(L_64);
		(L_64)->SetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_68), il2cpp_codegen_cast_double_to_int<int32_t>(L_72), 1);
		// path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y + 1] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_73 = __this->___path_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_74 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_74);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_75;
		L_75 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_74, NULL);
		NullCheck(L_75);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_76;
		L_76 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_75, NULL);
		float L_77 = L_76.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_78 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_78);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_79;
		L_79 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_78, NULL);
		NullCheck(L_79);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_80;
		L_80 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_79, NULL);
		float L_81 = L_80.___y_3;
		NullCheck(L_73);
		(L_73)->SetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_77), ((int32_t)il2cpp_codegen_add(il2cpp_codegen_cast_double_to_int<int32_t>(L_81), 1)), 1);
		// Vars.ball.transform.position = new Vector2(Vars.ball.transform.position.x, Vars.ball.transform.position.y + 1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_82 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_82);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_83;
		L_83 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_82, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_84 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_84);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_85;
		L_85 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_84, NULL);
		NullCheck(L_85);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_86;
		L_86 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_85, NULL);
		float L_87 = L_86.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_88 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_88);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_89;
		L_89 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_88, NULL);
		NullCheck(L_89);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_90;
		L_90 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_89, NULL);
		float L_91 = L_90.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_92;
		memset((&L_92), 0, sizeof(L_92));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_92), L_87, ((float)il2cpp_codegen_add(L_91, (1.0f))), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_93;
		L_93 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_92, NULL);
		NullCheck(L_83);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_83, L_93, NULL);
		goto IL_0496;
	}

IL_025f:
	{
		// else if (Vars.ball.transform.position.x - 1 >= 0 && path[(int)Vars.ball.transform.position.x - 1, (int)Vars.ball.transform.position.y] == 100)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_94 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_94);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_95;
		L_95 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_94, NULL);
		NullCheck(L_95);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_96;
		L_96 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_95, NULL);
		float L_97 = L_96.___x_2;
		if ((!(((float)((float)il2cpp_codegen_subtract(L_97, (1.0f)))) >= ((float)(0.0f)))))
		{
			goto IL_037b;
		}
	}
	{
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_98 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_99 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_99);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_100;
		L_100 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_99, NULL);
		NullCheck(L_100);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_101;
		L_101 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_100, NULL);
		float L_102 = L_101.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_103 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_103);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_104;
		L_104 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_103, NULL);
		NullCheck(L_104);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_105;
		L_105 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_104, NULL);
		float L_106 = L_105.___y_3;
		NullCheck(L_98);
		int32_t L_107;
		L_107 = (L_98)->GetAt(((int32_t)il2cpp_codegen_subtract(il2cpp_codegen_cast_double_to_int<int32_t>(L_102), 1)), il2cpp_codegen_cast_double_to_int<int32_t>(L_106));
		if ((!(((uint32_t)L_107) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_037b;
		}
	}
	{
		// path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_108 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_109 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_109);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_110;
		L_110 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_109, NULL);
		NullCheck(L_110);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_111;
		L_111 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_110, NULL);
		float L_112 = L_111.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_113 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_113);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_114;
		L_114 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_113, NULL);
		NullCheck(L_114);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_115;
		L_115 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_114, NULL);
		float L_116 = L_115.___y_3;
		NullCheck(L_108);
		(L_108)->SetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_112), il2cpp_codegen_cast_double_to_int<int32_t>(L_116), 1);
		// path[(int)Vars.ball.transform.position.x - 1, (int)Vars.ball.transform.position.y] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_117 = __this->___path_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_118 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_118);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_119;
		L_119 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_118, NULL);
		NullCheck(L_119);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_120;
		L_120 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_119, NULL);
		float L_121 = L_120.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_122 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_122);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_123;
		L_123 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_122, NULL);
		NullCheck(L_123);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_124;
		L_124 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_123, NULL);
		float L_125 = L_124.___y_3;
		NullCheck(L_117);
		(L_117)->SetAt(((int32_t)il2cpp_codegen_subtract(il2cpp_codegen_cast_double_to_int<int32_t>(L_121), 1)), il2cpp_codegen_cast_double_to_int<int32_t>(L_125), 1);
		// Vars.ball.transform.position = new Vector2(Vars.ball.transform.position.x - 1, Vars.ball.transform.position.y);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_126 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_126);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_127;
		L_127 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_126, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_128 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_128);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_129;
		L_129 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_128, NULL);
		NullCheck(L_129);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_130;
		L_130 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_129, NULL);
		float L_131 = L_130.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_132 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_132);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_133;
		L_133 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_132, NULL);
		NullCheck(L_133);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_134;
		L_134 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_133, NULL);
		float L_135 = L_134.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_136;
		memset((&L_136), 0, sizeof(L_136));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_136), ((float)il2cpp_codegen_subtract(L_131, (1.0f))), L_135, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_137;
		L_137 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_136, NULL);
		NullCheck(L_127);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_127, L_137, NULL);
		goto IL_0496;
	}

IL_037b:
	{
		// else if ((int)Vars.ball.transform.position.x + 1 < path.GetLength(0) && path[(int)Vars.ball.transform.position.x + 1, (int)Vars.ball.transform.position.y] == 100)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_138 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_138);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_139;
		L_139 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_138, NULL);
		NullCheck(L_139);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_140;
		L_140 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_139, NULL);
		float L_141 = L_140.___x_2;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_142 = __this->___path_2;
		NullCheck((RuntimeArray*)L_142);
		int32_t L_143;
		L_143 = Array_GetLength_mFE7A9FE891DE1E07795230BE09854441CDD0E935((RuntimeArray*)L_142, 0, NULL);
		if ((((int32_t)((int32_t)il2cpp_codegen_add(il2cpp_codegen_cast_double_to_int<int32_t>(L_141), 1))) >= ((int32_t)L_143)))
		{
			goto IL_0496;
		}
	}
	{
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_144 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_145 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_145);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_146;
		L_146 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_145, NULL);
		NullCheck(L_146);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_147;
		L_147 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_146, NULL);
		float L_148 = L_147.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_149 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_149);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_150;
		L_150 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_149, NULL);
		NullCheck(L_150);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_151;
		L_151 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_150, NULL);
		float L_152 = L_151.___y_3;
		NullCheck(L_144);
		int32_t L_153;
		L_153 = (L_144)->GetAt(((int32_t)il2cpp_codegen_add(il2cpp_codegen_cast_double_to_int<int32_t>(L_148), 1)), il2cpp_codegen_cast_double_to_int<int32_t>(L_152));
		if ((!(((uint32_t)L_153) == ((uint32_t)((int32_t)100)))))
		{
			goto IL_0496;
		}
	}
	{
		// path[(int)Vars.ball.transform.position.x, (int)Vars.ball.transform.position.y] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_154 = __this->___path_2;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_155 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_155);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_156;
		L_156 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_155, NULL);
		NullCheck(L_156);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_157;
		L_157 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_156, NULL);
		float L_158 = L_157.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_159 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_159);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_160;
		L_160 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_159, NULL);
		NullCheck(L_160);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_161;
		L_161 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_160, NULL);
		float L_162 = L_161.___y_3;
		NullCheck(L_154);
		(L_154)->SetAt(il2cpp_codegen_cast_double_to_int<int32_t>(L_158), il2cpp_codegen_cast_double_to_int<int32_t>(L_162), 1);
		// path[(int)Vars.ball.transform.position.x + 1, (int)Vars.ball.transform.position.y] = 1;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_163 = __this->___path_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_164 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_164);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_165;
		L_165 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_164, NULL);
		NullCheck(L_165);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_166;
		L_166 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_165, NULL);
		float L_167 = L_166.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_168 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_168);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_169;
		L_169 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_168, NULL);
		NullCheck(L_169);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_170;
		L_170 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_169, NULL);
		float L_171 = L_170.___y_3;
		NullCheck(L_163);
		(L_163)->SetAt(((int32_t)il2cpp_codegen_add(il2cpp_codegen_cast_double_to_int<int32_t>(L_167), 1)), il2cpp_codegen_cast_double_to_int<int32_t>(L_171), 1);
		// Vars.ball.transform.position = new Vector2(Vars.ball.transform.position.x + 1, Vars.ball.transform.position.y);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_172 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_172);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_173;
		L_173 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_172, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_174 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_174);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_175;
		L_175 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_174, NULL);
		NullCheck(L_175);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_176;
		L_176 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_175, NULL);
		float L_177 = L_176.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_178 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_178);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_179;
		L_179 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_178, NULL);
		NullCheck(L_179);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_180;
		L_180 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_179, NULL);
		float L_181 = L_180.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_182;
		memset((&L_182), 0, sizeof(L_182));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_182), ((float)il2cpp_codegen_add(L_177, (1.0f))), L_181, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_183;
		L_183 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_182, NULL);
		NullCheck(L_173);
		Transform_set_position_mA1A817124BB41B685043DED2A9BA48CDF37C4156(L_173, L_183, NULL);
	}

IL_0496:
	{
		// yield return new WaitForSeconds(0.02f);
		WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3* L_184 = (WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3*)il2cpp_codegen_object_new(WaitForSeconds_tF179DF251655B8DF044952E70A60DF4B358A3DD3_il2cpp_TypeInfo_var);
		NullCheck(L_184);
		WaitForSeconds__ctor_m579F95BADEDBAB4B3A7E302C6EE3995926EF2EFC(L_184, (0.0199999996f), NULL);
		__this->___U3CU3E2__current_1 = L_184;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_184);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_04af:
	{
		__this->___U3CU3E1__state_0 = (-1);
	}

IL_04b6:
	{
		// while (new Vector2(Vars.ball.transform.position.x, Vars.ball.transform.position.y) != new Vector2(xTarget, yTarget))
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_185 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_185);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_186;
		L_186 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_185, NULL);
		NullCheck(L_186);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_187;
		L_187 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_186, NULL);
		float L_188 = L_187.___x_2;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_189 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_189);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_190;
		L_190 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_189, NULL);
		NullCheck(L_190);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_191;
		L_191 = Transform_get_position_m69CD5FA214FDAE7BB701552943674846C220FDE1(L_190, NULL);
		float L_192 = L_191.___y_3;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_193;
		memset((&L_193), 0, sizeof(L_193));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_193), L_188, L_192, /*hidden argument*/NULL);
		int32_t L_194 = __this->___xTarget_3;
		int32_t L_195 = __this->___yTarget_4;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_196;
		memset((&L_196), 0, sizeof(L_196));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_196), ((float)L_194), ((float)L_195), /*hidden argument*/NULL);
		bool L_197;
		L_197 = Vector2_op_Inequality_mCF3935E28AC7B30B279F07F9321CC56718E1311A_inline(L_193, L_196, NULL);
		if (L_197)
		{
			goto IL_002c;
		}
	}
	{
		// Vars.isBallMoving = false;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___isBallMoving_8 = (bool)0;
		// Vars.fields[Vars.ballStartPosX, Vars.ballStartPosY] = 0;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_198 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_199 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5;
		int32_t L_200 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6;
		NullCheck(L_198);
		(L_198)->SetAt(L_199, L_200, 0);
		// Vars.fields[xTarget, yTarget] = ballColor;
		Int32U5BU2CU5D_t46F2694E7DAD7B2B05C940EC5B9DE04E40D0516F* L_201 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___fields_4;
		int32_t L_202 = __this->___xTarget_3;
		int32_t L_203 = __this->___yTarget_4;
		int32_t L_204 = __this->___ballColor_5;
		NullCheck(L_201);
		(L_201)->SetAt(L_202, L_203, L_204);
		// Vars.ballStartPosX = -1;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5 = (-1);
		// Vars.ball.transform.parent = null;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_205 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_205);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_206;
		L_206 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_205, NULL);
		NullCheck(L_206);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_206, (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*)NULL, NULL);
		// Vars.ball.transform.parent = GameObject.Find("Tile" + xTarget + "X" + yTarget).transform;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_207 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_207);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_208;
		L_208 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_207, NULL);
		int32_t* L_209 = (&__this->___xTarget_3);
		String_t* L_210;
		L_210 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_209, NULL);
		int32_t* L_211 = (&__this->___yTarget_4);
		String_t* L_212;
		L_212 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_211, NULL);
		String_t* L_213;
		L_213 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(_stringLiteralA12DF7E063F9EDD6CB3B99E1A6200411CA060DEE, L_210, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, L_212, NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_214;
		L_214 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_213, NULL);
		NullCheck(L_214);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_215;
		L_215 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_214, NULL);
		NullCheck(L_208);
		Transform_set_parent_m9BD5E563B539DD5BEC342736B03F97B38A243234(L_208, L_215, NULL);
		// CheckScore();
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_216 = V_1;
		NullCheck(L_216);
		BallsPathfinder_CheckScore_m6345AE27461D331492D27D28E16945CCF09411F5(L_216, NULL);
		// if (lastScore == Vars.score)
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_217 = V_1;
		NullCheck(L_217);
		int32_t L_218 = L_217->___lastScore_11;
		int32_t L_219 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		if ((!(((uint32_t)L_218) == ((uint32_t)L_219))))
		{
			goto IL_0606;
		}
	}
	{
		// if (Vars.ball.transform.parent.transform.Find("BallPlaceholder") != null)
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_220 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_220);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_221;
		L_221 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_220, NULL);
		NullCheck(L_221);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_222;
		L_222 = Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E(L_221, NULL);
		NullCheck(L_222);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_223;
		L_223 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_222, NULL);
		NullCheck(L_223);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_224;
		L_224 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_223, _stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_225;
		L_225 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_224, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_225)
		{
			goto IL_05fe;
		}
	}
	{
		// Destroy(Vars.ball.transform.parent.transform.Find("BallPlaceholder").gameObject);
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_226 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_226);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_227;
		L_227 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_226, NULL);
		NullCheck(L_227);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_228;
		L_228 = Transform_get_parent_m65354E28A4C94EC00EBCF03532F7B0718380791E(L_227, NULL);
		NullCheck(L_228);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_229;
		L_229 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(L_228, NULL);
		NullCheck(L_229);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_230;
		L_230 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_229, _stringLiteralFADBDE92D7CE84818473949E82C9F8ED88CCEDA0, NULL);
		NullCheck(L_230);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_231;
		L_231 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_230, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_231, NULL);
		// CreateNewBall(xTarget, yTarget);
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_232 = V_1;
		int32_t L_233 = __this->___xTarget_3;
		int32_t L_234 = __this->___yTarget_4;
		NullCheck(L_232);
		BallsPathfinder_CreateNewBall_m29425C07E0D9CE01502985D39A7ED770E2D94279(L_232, L_233, L_234, NULL);
	}

IL_05fe:
	{
		// CreateNewBalls();
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_235 = V_1;
		NullCheck(L_235);
		BallsPathfinder_CreateNewBalls_m39799563225761B4B7D54ECDD5BB1BCA9AAE76DD(L_235, NULL);
		goto IL_0625;
	}

IL_0606:
	{
		// GameObject.Find("BallPopSound").GetComponent<AudioSource>().Play();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_236;
		L_236 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral6271B5A613D8E9F509493567C383DD2671517B43, NULL);
		NullCheck(L_236);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_237;
		L_237 = GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A(L_236, GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		NullCheck(L_237);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_237, NULL);
		// lastScore = Vars.score;
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_238 = V_1;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_239 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		NullCheck(L_238);
		L_238->___lastScore_11 = L_239;
	}

IL_0625:
	{
		// }
		return (bool)0;
	}
}
// System.Object BallsPathfinder/<BallMovement>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBallMovementU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52A687D8880551C632BFC1DF724CC1315A100569 (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void BallsPathfinder/<BallMovement>d__29::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CBallMovementU3Ed__29_System_Collections_IEnumerator_Reset_m34433EBCFA168D9DDA8E486D0BADD873ACCAC264 (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NullCheck(L_0);
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CBallMovementU3Ed__29_System_Collections_IEnumerator_Reset_m34433EBCFA168D9DDA8E486D0BADD873ACCAC264_RuntimeMethod_var)));
	}
}
// System.Object BallsPathfinder/<BallMovement>d__29::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CBallMovementU3Ed__29_System_Collections_IEnumerator_get_Current_mFE6FF4878CA4A2449D73AF6D2CC7A747061D28F3 (U3CBallMovementU3Ed__29_tF7500A5D8020256140403C28324423B2B01DBE63* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DestroyExplosionParticle::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestroyExplosionParticle_OnEnable_mEE71A98ADC6316C035FFE6E7AFD26C760EBF7C3A (DestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(this.gameObject, 1f);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_m0E1B4CF8C29EB7FC8658C2C84C57F49C0DD12C91(L_0, (1.0f), NULL);
		// }
		return;
	}
}
// System.Void DestroyExplosionParticle::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DestroyExplosionParticle__ctor_m99F38967986788A00A1FFDE267649E2A6FDE4852 (DestroyExplosionParticle_t8C6AE6BF2AE53BB34D0974C01737852F0EEB3BD4* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Menus::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_Start_m757D84C515E8782E8A19DC7833F8BF0A15163499 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCD078B3D013E9178455A33BB9E00BF4A1C4A52EE);
		s_Il2CppMethodInitialized = true;
	}
	{
		// buttonSound = GameObject.Find("ButtonSound").GetComponent<AudioSource>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralCD078B3D013E9178455A33BB9E00BF4A1C4A52EE, NULL);
		NullCheck(L_0);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_1;
		L_1 = GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A(L_0, GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		__this->___buttonSound_10 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___buttonSound_10), (void*)L_1);
		// }
		return;
	}
}
// System.Void Menus::UpdateTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_UpdateTimer_mB8AD879D0B4372CD0C5F32370D09D81096481148 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE5C37D372367C69DCD30FE688631A1B0CE49EA73);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// seconds += Time.deltaTime;
		float L_0 = __this->___seconds_16;
		float L_1;
		L_1 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		__this->___seconds_16 = ((float)il2cpp_codegen_add(L_0, L_1));
		// timer.text = hour + ":" + minutes.ToString("00") + ":" + ((int)seconds).ToString("00");
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_2 = __this->___timer_15;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_3 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = L_3;
		int32_t* L_5 = (&__this->___hour_18);
		String_t* L_6;
		L_6 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5(L_5, NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_6);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_7 = L_4;
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = L_7;
		int32_t* L_9 = (&__this->___minutes_17);
		String_t* L_10;
		L_10 = Int32_ToString_m967AECC237535C552A97A80C7875E31B98496CA9(L_9, _stringLiteralE5C37D372367C69DCD30FE688631A1B0CE49EA73, NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_10);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_11 = L_8;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, _stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral876C4B39B6E4D0187090400768899C71D99DE90D);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12 = L_11;
		float L_13 = __this->___seconds_16;
		V_0 = il2cpp_codegen_cast_double_to_int<int32_t>(L_13);
		String_t* L_14;
		L_14 = Int32_ToString_m967AECC237535C552A97A80C7875E31B98496CA9((&V_0), _stringLiteralE5C37D372367C69DCD30FE688631A1B0CE49EA73, NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)L_14);
		String_t* L_15;
		L_15 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_12, NULL);
		NullCheck(L_2);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_15);
		// if (seconds >= 60)
		float L_16 = __this->___seconds_16;
		if ((!(((float)L_16) >= ((float)(60.0f)))))
		{
			goto IL_0097;
		}
	}
	{
		// minutes++;
		int32_t L_17 = __this->___minutes_17;
		__this->___minutes_17 = ((int32_t)il2cpp_codegen_add(L_17, 1));
		// seconds = 0;
		__this->___seconds_16 = (0.0f);
		return;
	}

IL_0097:
	{
		// else if (minutes >= 60)
		int32_t L_18 = __this->___minutes_17;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)60))))
		{
			goto IL_00b6;
		}
	}
	{
		// hour++;
		int32_t L_19 = __this->___hour_18;
		__this->___hour_18 = ((int32_t)il2cpp_codegen_add(L_19, 1));
		// minutes = 0;
		__this->___minutes_17 = 0;
	}

IL_00b6:
	{
		// }
		return;
	}
}
// System.Void Menus::ResetTimer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_ResetTimer_m5A959F0FF4A0C9A61F36E21DB58E70EA725D0D18 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		// seconds = 0.0f;
		__this->___seconds_16 = (0.0f);
		// minutes = 0;
		__this->___minutes_17 = 0;
		// hour = 0;
		__this->___hour_18 = 0;
		// UpdateTimer();
		Menus_UpdateTimer_mB8AD879D0B4372CD0C5F32370D09D81096481148(__this, NULL);
		// }
		return;
	}
}
// System.Void Menus::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_Update_m48773A3B354E97888B0578CD3FE8DC889B7BD2A7 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		// UpdateTimer();
		Menus_UpdateTimer_mB8AD879D0B4372CD0C5F32370D09D81096481148(__this, NULL);
		// }
		return;
	}
}
// System.Void Menus::StartGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_StartGame_m2B84735B2B5A9DA804212E62FBFF2E9FC7F69B21 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral075488768B36FC6B56A29920D44014060332E6C1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2B4CB989CF705BDB31F2D19B8AB3AB7081C4C9DB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralED35FF3F063D29A026AE09F18392D9C7537823F2);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// mainMenuUI.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___mainMenuUI_4;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)0, NULL);
		// gameplayUI.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___gameplayUI_5;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// pathFinder.enabled = true;
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_2 = __this->___pathFinder_9;
		NullCheck(L_2);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_2, (bool)1, NULL);
		// score.text = "SCORE: " + Vars.score;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_3 = __this->___score_13;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		String_t* L_4;
		L_4 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9), NULL);
		String_t* L_5;
		L_5 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralED35FF3F063D29A026AE09F18392D9C7537823F2, L_4, NULL);
		NullCheck(L_3);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_5);
		// bestScore.text = "BEST: " + PlayerPrefs.GetInt("BestScore");
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___bestScore_14;
		int32_t L_7;
		L_7 = PlayerPrefs_GetInt_m35C13A87BBC7907554CE5405EB5D00CF85E7457B(_stringLiteral2B4CB989CF705BDB31F2D19B8AB3AB7081C4C9DB, NULL);
		V_0 = L_7;
		String_t* L_8;
		L_8 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_0), NULL);
		String_t* L_9;
		L_9 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteral075488768B36FC6B56A29920D44014060332E6C1, L_8, NULL);
		NullCheck(L_6);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
		// pathFinder.numberOfRows = 9;
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_10 = __this->___pathFinder_9;
		NullCheck(L_10);
		L_10->___numberOfRows_4 = ((int32_t)9);
		// pathFinder.numberOfColumns = 9;
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_11 = __this->___pathFinder_9;
		NullCheck(L_11);
		L_11->___numberOfColumns_5 = ((int32_t)9);
		// }
		return;
	}
}
// System.Void Menus::ExitGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_ExitGame_m998AC26A505A814417AB0ACD87A1D612FC7D0521 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		// Application.Quit();
		Application_Quit_m965C6D4CA85A24DD95B347D22837074F19C58134(NULL);
		// }
		return;
	}
}
// System.Void Menus::PauseMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_PauseMenu_mD2F16EB6B188ACC9B67579DF10D152C1343A5224 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		// buttonSound.Play();
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0 = __this->___buttonSound_10;
		NullCheck(L_0);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_0, NULL);
		// pauseMenuUI.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___pauseMenuUI_6;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Menus::ClosePauseMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_ClosePauseMenu_mDC94D204451B98F7A1D3843D54F73316AFC92CE5 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		// buttonSound.Play();
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_0 = __this->___buttonSound_10;
		NullCheck(L_0);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_0, NULL);
		// pauseMenuUI.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___pauseMenuUI_6;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Menus::GameOverMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_GameOverMenu_m55C974EC861F3C28CB8BAAF89883DF51C0D291F7 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		// gameOverMenuUI.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___gameOverMenuUI_7;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// }
		return;
	}
}
// System.Void Menus::RestartTheGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_RestartTheGame_m583C0A2D7D72C41F8D91B5B0492B74F7CCDBD69B (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA74ACAA1F61DE0EB348EC03946685B0B6270CB36);
		s_Il2CppMethodInitialized = true;
	}
	{
		// pathFinder.enabled = false;
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_0 = __this->___pathFinder_9;
		NullCheck(L_0);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_0, (bool)0, NULL);
		// DestroyAllTiles();
		Menus_DestroyAllTiles_m1D62BB3B9F661210BF1800BC086A4E4AC5558891(__this, NULL);
		// Vars.ResetAllVariables();
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Vars_ResetAllVariables_m038B64E49DA06DAC269D70A416CD22A1B6A80D9D(NULL);
		// pauseMenuUI.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___pauseMenuUI_6;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// gameOverMenuUI.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = __this->___gameOverMenuUI_7;
		NullCheck(L_2);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)0, NULL);
		// ResetTimer();
		Menus_ResetTimer_m5A959F0FF4A0C9A61F36E21DB58E70EA725D0D18(__this, NULL);
		// Invoke("Reset", 0);
		MonoBehaviour_Invoke_mF724350C59362B0F1BFE26383209A274A29A63FB(__this, _stringLiteralA74ACAA1F61DE0EB348EC03946685B0B6270CB36, (0.0f), NULL);
		// }
		return;
	}
}
// System.Void Menus::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_Reset_m5CD3E64A48477CCE622CF53F3D3BD135377D08A0 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		// StartGame();
		Menus_StartGame_m2B84735B2B5A9DA804212E62FBFF2E9FC7F69B21(__this, NULL);
		// }
		return;
	}
}
// System.Void Menus::DestroyAllTiles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_DestroyAllTiles_m1D62BB3B9F661210BF1800BC086A4E4AC5558891 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD0DABD215A28AA39516A755A399D76F8E7E493C0);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	{
		// Transform tilesGameObject = GameObject.Find("Tiles").transform;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralD0DABD215A28AA39516A755A399D76F8E7E493C0, NULL);
		NullCheck(L_0);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_0, NULL);
		// foreach (Transform child in tilesGameObject)
		NullCheck(L_1);
		RuntimeObject* L_2;
		L_2 = Transform_GetEnumerator_mA7E1C882ACA0C33E284711CD09971DEA3FFEF404(L_1, NULL);
		V_0 = L_2;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0036:
			{// begin finally (depth: 1)
				{
					RuntimeObject* L_3 = V_0;
					V_1 = ((RuntimeObject*)IsInst((RuntimeObject*)L_3, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var));
					RuntimeObject* L_4 = V_1;
					if (!L_4)
					{
						goto IL_0046;
					}
				}
				{
					RuntimeObject* L_5 = V_1;
					NullCheck(L_5);
					InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_5);
				}

IL_0046:
				{
					return;
				}
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_002c_1;
			}

IL_0017_1:
			{
				// foreach (Transform child in tilesGameObject)
				RuntimeObject* L_6 = V_0;
				NullCheck(L_6);
				RuntimeObject* L_7;
				L_7 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_6);
				// GameObject.Destroy(child.gameObject);
				NullCheck(((Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*)CastclassClass((RuntimeObject*)L_7, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var)));
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8;
				L_8 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(((Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1*)CastclassClass((RuntimeObject*)L_7, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1_il2cpp_TypeInfo_var)), NULL);
				il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
				Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_8, NULL);
			}

IL_002c_1:
			{
				// foreach (Transform child in tilesGameObject)
				RuntimeObject* L_9 = V_0;
				NullCheck(L_9);
				bool L_10;
				L_10 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA_il2cpp_TypeInfo_var, L_9);
				if (L_10)
				{
					goto IL_0017_1;
				}
			}
			{
				goto IL_0047;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0047:
	{
		// }
		return;
	}
}
// System.Void Menus::BackToTheMainMenu()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_BackToTheMainMenu_m66E0F80462B2363911C0D06CD5675793BD536FF8 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// DestroyAllTiles();
		Menus_DestroyAllTiles_m1D62BB3B9F661210BF1800BC086A4E4AC5558891(__this, NULL);
		// mainMenuUI.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0 = __this->___mainMenuUI_4;
		NullCheck(L_0);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_0, (bool)1, NULL);
		// gameplayUI.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = __this->___gameplayUI_5;
		NullCheck(L_1);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// pathFinder.enabled = false;
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_2 = __this->___pathFinder_9;
		NullCheck(L_2);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_2, (bool)0, NULL);
		// Vars.ResetAllVariables();
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		Vars_ResetAllVariables_m038B64E49DA06DAC269D70A416CD22A1B6A80D9D(NULL);
		// mainButtons.transform.localScale = new Vector2(1, 1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_3 = __this->___mainButtons_8;
		NullCheck(L_3);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_3, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_5;
		memset((&L_5), 0, sizeof(L_5));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_5), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_5, NULL);
		NullCheck(L_4);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_4, L_6, NULL);
		// pauseMenuUI.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_7 = __this->___pauseMenuUI_6;
		NullCheck(L_7);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_7, (bool)0, NULL);
		// gameOverMenuUI.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8 = __this->___gameOverMenuUI_7;
		NullCheck(L_8);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_8, (bool)0, NULL);
		// }
		return;
	}
}
// System.Void Menus::UpdateNextWaveBallsColor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_UpdateNextWaveBallsColor_mDDCDA9C3B75918F26B537B10C3C97DFC272BE1A5 (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, int32_t ___ball0, int32_t ___color1, const RuntimeMethod* method) 
{
	{
		// nextWaveBalls[ball].sprite = balls[color];
		ImageU5BU5D_t8869694C217655DA7B1315DC02C80F1308B78B78* L_0 = __this->___nextWaveBalls_12;
		int32_t L_1 = ___ball0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		SpriteU5BU5D_tCEE379E10CAD9DBFA770B331480592548ED0EA1B* L_4 = __this->___balls_11;
		int32_t L_5 = ___color1;
		NullCheck(L_4);
		int32_t L_6 = L_5;
		Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck(L_3);
		Image_set_sprite_mC0C248340BA27AAEE56855A3FAFA0D8CA12956DE(L_3, L_7, NULL);
		// }
		return;
	}
}
// System.Void Menus::UpdateScore()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus_UpdateScore_m6D7C68AE360246A76F901FE59837276BF601E8DC (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral075488768B36FC6B56A29920D44014060332E6C1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2B4CB989CF705BDB31F2D19B8AB3AB7081C4C9DB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralED35FF3F063D29A026AE09F18392D9C7537823F2);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// score.text = "SCORE: " + Vars.score;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_0 = __this->___score_13;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9), NULL);
		String_t* L_2;
		L_2 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralED35FF3F063D29A026AE09F18392D9C7537823F2, L_1, NULL);
		NullCheck(L_0);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		// if (Vars.score > PlayerPrefs.GetInt("BestScore"))
		int32_t L_3 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		int32_t L_4;
		L_4 = PlayerPrefs_GetInt_m35C13A87BBC7907554CE5405EB5D00CF85E7457B(_stringLiteral2B4CB989CF705BDB31F2D19B8AB3AB7081C4C9DB, NULL);
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_0066;
		}
	}
	{
		// PlayerPrefs.SetInt("BestScore", Vars.score);
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_5 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9;
		PlayerPrefs_SetInt_mDC9617BFD56FEC670626A1002D9A5FE963D8D175(_stringLiteral2B4CB989CF705BDB31F2D19B8AB3AB7081C4C9DB, L_5, NULL);
		// bestScore.text = "BEST: " + PlayerPrefs.GetInt("BestScore");
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_6 = __this->___bestScore_14;
		int32_t L_7;
		L_7 = PlayerPrefs_GetInt_m35C13A87BBC7907554CE5405EB5D00CF85E7457B(_stringLiteral2B4CB989CF705BDB31F2D19B8AB3AB7081C4C9DB, NULL);
		V_0 = L_7;
		String_t* L_8;
		L_8 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_0), NULL);
		String_t* L_9;
		L_9 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteral075488768B36FC6B56A29920D44014060332E6C1, L_8, NULL);
		NullCheck(L_6);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_9);
	}

IL_0066:
	{
		// }
		return;
	}
}
// System.Void Menus::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Menus__ctor_mBD8483151FBC884103329A35949A7354D29BED9B (Menus_t48EA0F4A396C6670CB3B6C7DE6C2F331CA03D540* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SelectedBallAnimation::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SelectedBallAnimation_OnEnable_m11E98BDF5DF733E96C3F6BA2FF70A33391AB9004 (SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* __this, const RuntimeMethod* method) 
{
	{
		// up = false;
		__this->___up_4 = (bool)0;
		// scale = 1;
		__this->___scale_5 = (1.0f);
		// }
		return;
	}
}
// System.Void SelectedBallAnimation::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SelectedBallAnimation_Update_mEE60CBB1A15C030ACC7D403ADCCCB38D5152E649 (SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* __this, const RuntimeMethod* method) 
{
	{
		// if(up) {
		bool L_0 = __this->___up_4;
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		// scale += Time.deltaTime / 3;
		float L_1 = __this->___scale_5;
		float L_2;
		L_2 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		__this->___scale_5 = ((float)il2cpp_codegen_add(L_1, ((float)(L_2/(3.0f)))));
		// if(scale >= 1) {
		float L_3 = __this->___scale_5;
		if ((!(((float)L_3) >= ((float)(1.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		// up = false;
		__this->___up_4 = (bool)0;
		goto IL_0062;
	}

IL_0036:
	{
		// scale -= Time.deltaTime / 3;
		float L_4 = __this->___scale_5;
		float L_5;
		L_5 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		__this->___scale_5 = ((float)il2cpp_codegen_subtract(L_4, ((float)(L_5/(3.0f)))));
		// if(scale <= 0.8f) {
		float L_6 = __this->___scale_5;
		if ((!(((float)L_6) <= ((float)(0.800000012f)))))
		{
			goto IL_0062;
		}
	}
	{
		// up = true;
		__this->___up_4 = (bool)1;
	}

IL_0062:
	{
		// transform.localScale = new Vector2(scale, scale);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_7;
		L_7 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		float L_8 = __this->___scale_5;
		float L_9 = __this->___scale_5;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_10), L_8, L_9, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_11;
		L_11 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_10, NULL);
		NullCheck(L_7);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_7, L_11, NULL);
		// }
		return;
	}
}
// System.Void SelectedBallAnimation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SelectedBallAnimation__ctor_m9CA2B55CB46169D9D43EEDF4582B9A52E776BD3B (SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* __this, const RuntimeMethod* method) 
{
	{
		// private float scale = 1;
		__this->___scale_5 = (1.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Tile::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tile_OnMouseDown_m15CC374FC44376602BDD7405A7125F9AB2542ED0 (Tile_t192D2F5511792792FB74C37341AFAA9F6B77AE64* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisBallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990_mEC272194416C4A4FAD035A00B789D99ABB9B83AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36282FAC116D9FD6B37CC425310E1A8510F08A53);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5B68E4FC12CF26A2805AD3EC3EDA6F38D0B99347);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral62CE1AE2C494852B7905BEEC1F46C7400044A2C7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		// if(Vars.isBallMoving) return;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		bool L_0 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___isBallMoving_8;
		if (!L_0)
		{
			goto IL_0008;
		}
	}
	{
		// if(Vars.isBallMoving) return;
		return;
	}

IL_0008:
	{
		// string name = this.gameObject.name;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(__this, NULL);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = Object_get_name_mAC2F6B897CF1303BA4249B4CB55271AFACBB6392(L_1, NULL);
		// int from = name.IndexOf("e") + "e".Length;
		String_t* L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = String_IndexOf_m69E9BDAFD93767C85A7FF861B453415D3B4A200F(L_3, _stringLiteral36282FAC116D9FD6B37CC425310E1A8510F08A53, NULL);
		NullCheck(_stringLiteral36282FAC116D9FD6B37CC425310E1A8510F08A53);
		int32_t L_5;
		L_5 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteral36282FAC116D9FD6B37CC425310E1A8510F08A53, NULL);
		V_0 = ((int32_t)il2cpp_codegen_add(L_4, L_5));
		// int to = name.LastIndexOf("X");
		String_t* L_6 = L_3;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = String_LastIndexOf_m8923DBD89F2B3E5A34190B038B48F402E0C17E40(L_6, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, NULL);
		V_1 = L_7;
		// string x = name.Substring(from, to - from);
		String_t* L_8 = L_6;
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		NullCheck(L_8);
		String_t* L_12;
		L_12 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_8, L_9, ((int32_t)il2cpp_codegen_subtract(L_10, L_11)), NULL);
		V_2 = L_12;
		// from = name.IndexOf("X") + "X".Length;
		String_t* L_13 = L_8;
		NullCheck(L_13);
		int32_t L_14;
		L_14 = String_IndexOf_m69E9BDAFD93767C85A7FF861B453415D3B4A200F(L_13, _stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, NULL);
		NullCheck(_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE);
		int32_t L_15;
		L_15 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(_stringLiteralD5D2875F228D651E1289522AEAAB8C492001C1BE, NULL);
		V_0 = ((int32_t)il2cpp_codegen_add(L_14, L_15));
		// to = name.Length;
		String_t* L_16 = L_13;
		NullCheck(L_16);
		int32_t L_17;
		L_17 = String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline(L_16, NULL);
		V_1 = L_17;
		// string y = name.Substring(from, to - from);
		int32_t L_18 = V_0;
		int32_t L_19 = V_1;
		int32_t L_20 = V_0;
		NullCheck(L_16);
		String_t* L_21;
		L_21 = String_Substring_mB1D94F47935D22E130FF2C01DBB6A4135FBB76CE(L_16, L_18, ((int32_t)il2cpp_codegen_subtract(L_19, L_20)), NULL);
		V_3 = L_21;
		// if(transform.Find("Ball") != null) {
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_22;
		L_22 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_22);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_23;
		L_23 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_22, _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, NULL);
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_24;
		L_24 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_23, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_24)
		{
			goto IL_0119;
		}
	}
	{
		// if(Vars.ball != null) {
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_25 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		bool L_26;
		L_26 = Object_op_Inequality_m4D656395C27694A7F33F5AA8DE80A7AAF9E20BA7(L_25, (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C*)NULL, NULL);
		if (!L_26)
		{
			goto IL_00c4;
		}
	}
	{
		// Vars.ball.GetComponent<SelectedBallAnimation> ().enabled = false;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_27);
		SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* L_28;
		L_28 = GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070(L_27, GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070_RuntimeMethod_var);
		NullCheck(L_28);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_28, (bool)0, NULL);
		// Vars.ball.transform.localScale = new Vector2(1, 1);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_29 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_29);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_30;
		L_30 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_29, NULL);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_31;
		memset((&L_31), 0, sizeof(L_31));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_31), (1.0f), (1.0f), /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_32;
		L_32 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_31, NULL);
		NullCheck(L_30);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_30, L_32, NULL);
	}

IL_00c4:
	{
		// Vars.ballStartPosX = Int32.Parse(x);
		String_t* L_33 = V_2;
		int32_t L_34;
		L_34 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_33, NULL);
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5 = L_34;
		// Vars.ballStartPosY = Int32.Parse(y);
		String_t* L_35 = V_3;
		int32_t L_36;
		L_36 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_35, NULL);
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6 = L_36;
		// Vars.ball = transform.Find("Ball").gameObject;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_37;
		L_37 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		NullCheck(L_37);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_38;
		L_38 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_37, _stringLiteralAF4FE737DB07A38DC7B9A34B4BA555906EC0EFA9, NULL);
		NullCheck(L_38);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_39;
		L_39 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_38, NULL);
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7 = L_39;
		Il2CppCodeGenWriteBarrier((void**)(&((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7), (void*)L_39);
		// Vars.ball.GetComponent<SelectedBallAnimation> ().enabled = true;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_40 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		NullCheck(L_40);
		SelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A* L_41;
		L_41 = GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070(L_40, GameObject_GetComponent_TisSelectedBallAnimation_t46226325AB62C2F2B41BA7BEB11F965308FEB17A_m8BBA94D955AA3CBED7B90DA7CAF176AB52E17070_RuntimeMethod_var);
		NullCheck(L_41);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_41, (bool)1, NULL);
		// GameObject.Find("BallSelectSound").GetComponent<AudioSource> ().Play();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_42;
		L_42 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral62CE1AE2C494852B7905BEEC1F46C7400044A2C7, NULL);
		NullCheck(L_42);
		AudioSource_t871AC2272F896738252F04EE949AEF5B241D3299* L_43;
		L_43 = GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A(L_42, GameObject_GetComponent_TisAudioSource_t871AC2272F896738252F04EE949AEF5B241D3299_m4F30DAB8E5B57E8DB6BD8C0C3BB11CCB57690C4A_RuntimeMethod_var);
		NullCheck(L_43);
		AudioSource_Play_m95DF07111C61D0E0F00257A00384D31531D590C3(L_43, NULL);
		return;
	}

IL_0119:
	{
		// if(Vars.ballStartPosX == -1) return;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		int32_t L_44 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5;
		if ((!(((uint32_t)L_44) == ((uint32_t)(-1)))))
		{
			goto IL_0122;
		}
	}
	{
		// if(Vars.ballStartPosX == -1) return;
		return;
	}

IL_0122:
	{
		// int xPos = Int32.Parse(x);
		String_t* L_45 = V_2;
		int32_t L_46;
		L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
		V_4 = L_46;
		// int yPos = Int32.Parse(y);
		String_t* L_47 = V_3;
		int32_t L_48;
		L_48 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_47, NULL);
		V_5 = L_48;
		// GameObject.Find("GameManager").GetComponent<BallsPathfinder> ().InitializeBallMovement(Vars.ball, xPos, yPos);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_49;
		L_49 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral5B68E4FC12CF26A2805AD3EC3EDA6F38D0B99347, NULL);
		NullCheck(L_49);
		BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* L_50;
		L_50 = GameObject_GetComponent_TisBallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990_mEC272194416C4A4FAD035A00B789D99ABB9B83AE(L_49, GameObject_GetComponent_TisBallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990_mEC272194416C4A4FAD035A00B789D99ABB9B83AE_RuntimeMethod_var);
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_51 = ((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ball_7;
		int32_t L_52 = V_4;
		int32_t L_53 = V_5;
		NullCheck(L_50);
		BallsPathfinder_InitializeBallMovement_m9E10C61F99868F73E615449DF46689DD33CD340B(L_50, L_51, L_52, L_53, NULL);
		// }
		return;
	}
}
// System.Void Tile::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tile__ctor_mB2C904B47040471552C938AE751AC0BF80B369E7 (Tile_t192D2F5511792792FB74C37341AFAA9F6B77AE64* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Vars::ResetAllVariables()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vars_ResetAllVariables_m038B64E49DA06DAC269D70A416CD22A1B6A80D9D (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ballStartPosX = -1;
		il2cpp_codegen_runtime_class_init_inline(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5 = (-1);
		// ballStartPosY = -1;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6 = (-1);
		// isBallMoving = false;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___isBallMoving_8 = (bool)0;
		// score = 0;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9 = 0;
		// }
		return;
	}
}
// System.Void Vars::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vars__ctor_mE9D35ECC8033B60F9515456B41A5389407F0E43C (Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void Vars::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vars__cctor_m82F628B1BC3817FC1D99FF688094BC0B3F5C010F (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static int ballStartPosX = -1;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosX_5 = (-1);
		// public static int ballStartPosY = -1;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___ballStartPosY_6 = (-1);
		// public static bool isBallMoving = false;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___isBallMoving_8 = (bool)0;
		// public static int score = 0;
		((Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_StaticFields*)il2cpp_codegen_static_fields_for(Vars_t0D9EF4E8B9B55DC3FDC20A7A4FBA2B838214B752_il2cpp_TypeInfo_var))->___score_9 = 0;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZoomInAnimation::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomInAnimation_OnEnable_mEE1B958AC38DF423FE2938D26828475E1A9A0212 (ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431* __this, const RuntimeMethod* method) 
{
	{
		// scale = 0;
		__this->___scale_5 = (0.0f);
		// }
		return;
	}
}
// System.Void ZoomInAnimation::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomInAnimation_Update_mBB3D4C37ACD4A99B6371A93372701BBAA9BB5D5D (ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431_mF70B3A761AB9683B1A957B7117C21A3FA5BDC439_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// scale += Time.deltaTime * 3;
		float L_0 = __this->___scale_5;
		float L_1;
		L_1 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		__this->___scale_5 = ((float)il2cpp_codegen_add(L_0, ((float)il2cpp_codegen_multiply(L_1, (3.0f)))));
		// if(scale >= 1) {
		float L_2 = __this->___scale_5;
		if ((!(((float)L_2) >= ((float)(1.0f)))))
		{
			goto IL_005d;
		}
	}
	{
		// scale = 1;
		__this->___scale_5 = (1.0f);
		// rectTransform.localScale = new Vector2(scale, scale);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_3 = __this->___rectTransform_4;
		float L_4 = __this->___scale_5;
		float L_5 = __this->___scale_5;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), L_4, L_5, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_6, NULL);
		NullCheck(L_3);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_3, L_7, NULL);
		// GetComponent<ZoomInAnimation> ().enabled = false;
		ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431* L_8;
		L_8 = Component_GetComponent_TisZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431_mF70B3A761AB9683B1A957B7117C21A3FA5BDC439(__this, Component_GetComponent_TisZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431_mF70B3A761AB9683B1A957B7117C21A3FA5BDC439_RuntimeMethod_var);
		NullCheck(L_8);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_8, (bool)0, NULL);
	}

IL_005d:
	{
		// rectTransform.localScale = new Vector2(scale, scale);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_9 = __this->___rectTransform_4;
		float L_10 = __this->___scale_5;
		float L_11 = __this->___scale_5;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_12), L_10, L_11, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_12, NULL);
		NullCheck(L_9);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_9, L_13, NULL);
		// }
		return;
	}
}
// System.Void ZoomInAnimation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomInAnimation__ctor_m30A2A63AAD67E93737FAA34F1FCBA885D8A913FB (ZoomInAnimation_tF0B91796AAC260A80450AB446325AE111C11E431* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ZoomOutAnimation::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomOutAnimation_OnEnable_mAB26ED292376031B221E40BE0B1D813CA451711B (ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9* __this, const RuntimeMethod* method) 
{
	{
		// scale = 1;
		__this->___scale_5 = (1.0f);
		// }
		return;
	}
}
// System.Void ZoomOutAnimation::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomOutAnimation_Update_mBF9261FCF85B767BBCD69F83E41EBC5B2933B9A2 (ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9_m73FE44109D7EC9A0AE531876384FF3B9E8FB1CB2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// scale -= Time.deltaTime * 3;
		float L_0 = __this->___scale_5;
		float L_1;
		L_1 = Time_get_deltaTime_m7AB6BFA101D83E1D8F2EF3D5A128AEE9DDBF1A6D(NULL);
		__this->___scale_5 = ((float)il2cpp_codegen_subtract(L_0, ((float)il2cpp_codegen_multiply(L_1, (3.0f)))));
		// if(scale <= 0) {
		float L_2 = __this->___scale_5;
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_005d;
		}
	}
	{
		// scale = 0;
		__this->___scale_5 = (0.0f);
		// rectTransform.localScale = new Vector2(scale, scale);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_3 = __this->___rectTransform_4;
		float L_4 = __this->___scale_5;
		float L_5 = __this->___scale_5;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_6), L_4, L_5, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_7;
		L_7 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_6, NULL);
		NullCheck(L_3);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_3, L_7, NULL);
		// GetComponent<ZoomOutAnimation> ().enabled = false;
		ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9* L_8;
		L_8 = Component_GetComponent_TisZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9_m73FE44109D7EC9A0AE531876384FF3B9E8FB1CB2(__this, Component_GetComponent_TisZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9_m73FE44109D7EC9A0AE531876384FF3B9E8FB1CB2_RuntimeMethod_var);
		NullCheck(L_8);
		Behaviour_set_enabled_mF1DCFE60EB09E0529FE9476CA804A3AA2D72B16A(L_8, (bool)0, NULL);
	}

IL_005d:
	{
		// rectTransform.localScale = new Vector2(scale, scale);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_9 = __this->___rectTransform_4;
		float L_10 = __this->___scale_5;
		float L_11 = __this->___scale_5;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_12), L_10, L_11, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_13;
		L_13 = Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline(L_12, NULL);
		NullCheck(L_9);
		Transform_set_localScale_mBA79E811BAF6C47B80FF76414C12B47B3CD03633(L_9, L_13, NULL);
		// }
		return;
	}
}
// System.Void ZoomOutAnimation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoomOutAnimation__ctor_m1D7BF514B776D4FC62A3755AB01B0B7CEFBA139D (ZoomOutAnimation_t308D55D3E2425DDE0144C0FCB0724903017B2ED9* __this, const RuntimeMethod* method) 
{
	{
		// private float scale = 1;
		__this->___scale_5 = (1.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector2_op_Implicit_mCD214B04BC52AED3C89C3BEF664B6247E5F8954A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___v0, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___v0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___v0;
		float L_3 = L_2.___y_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t BallsPathfinder_get_Cols_m2D237BCDE09D2ED862D8855D71B86BDBCDE01C14_inline (BallsPathfinder_tEA245BB38DC6E03579446AF86699A1261EBD4990* __this, const RuntimeMethod* method) 
{
	{
		// get { return numberOfColumns; }
		int32_t L_0 = __this->___numberOfColumns_5;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Inequality_mCF3935E28AC7B30B279F07F9321CC56718E1311A_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) 
{
	bool V_0 = false;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___lhs0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1 = ___rhs1;
		bool L_2;
		L_2 = Vector2_op_Equality_m5447BF12C18339431AB8AF02FA463C543D88D463_inline(L_0, L_1, NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m42625D67623FA5CC7A44D47425CE86FB946542D2_inline (String_t* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = __this->____stringLength_4;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_m5447BF12C18339431AB8AF02FA463C543D88D463_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___lhs0, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rhs1, const RuntimeMethod* method) 
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_0 = ___lhs0;
		float L_1 = L_0.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_2 = ___rhs1;
		float L_3 = L_2.___x_0;
		V_0 = ((float)il2cpp_codegen_subtract(L_1, L_3));
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_4 = ___lhs0;
		float L_5 = L_4.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_6 = ___rhs1;
		float L_7 = L_6.___y_1;
		V_1 = ((float)il2cpp_codegen_subtract(L_5, L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		V_2 = (bool)((((float)((float)il2cpp_codegen_add(((float)il2cpp_codegen_multiply(L_8, L_9)), ((float)il2cpp_codegen_multiply(L_10, L_11))))) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_002e;
	}

IL_002e:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
